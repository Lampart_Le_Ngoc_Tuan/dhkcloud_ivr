<?php


class MC_Model extends CI_Model {

    protected $_ci;

    public function __construct()
    {
        parent::__construct();
        $this->_ci =& get_instance();
    }


    /**
     * @param $table
     * @param $params
     * @return bool
     * @author chieng_minh
     * @since 2019-11-06
     * [CRE][49728][51918]
     */
    public function _insert($table, $params) {

        if(!$table) {
            return false;
        }

        if(!$params) {
            return false;
        }

        if(!isset($params['lastup_account_id'])) {
            $params['lastup_account_id'] = $this->_get_user_id();
        }

        $now = $this->_db_now();

        if(!isset($params['create_datetime'])) {
            $params['create_datetime'] = $now;
        }

        if($this->db->insert($table, $params)) {
            return $this->db->insert_id();
        }
        return false;
    }

    /**
     * @param array $table
     * @param array $conditions
     * @param array $params
     * @return bool
     * @author chieng_minh
     * @since 2019-11-06
     * [CRE][49728][51918]
     */
    public function _update($table = array(), $conditions = array(), $params = array()) {

        if(!$table) {
            return false;
        }

        if(!$params || !$conditions) {
            return false;
        }

        if(!isset($params['lastup_account_id'])) {
            $params['lastup_account_id'] = $this->_get_user_id();
        }

        $now = $this->_db_now();

        if(!isset($params['create_datetime'])) {
            $params['create_datetime'] = $now;
        }

        if(!isset($params['lastup_datetime'])) {
            $params['lastup_datetime'] = $now;
        }

        foreach ($conditions as $key => $value) {
            $this->db->where($key, $value);
        }

        if($this->db->update($table, $params)) {
            return true;
        }
        return false;
    }

    /**
     * @param array $table
     * @param array $conditions
     * @param string $select_field
     * @return bool
     * @author chieng_minh
     * @since 2019-11-06
     * [CRE][49728][51918]
     */
    public function _select($table = array(), $conditions = array(), $select_field = '', $order_by = '', $return_array = false) {

        if(!$table) {
            return false;
        }

        if($select_field) {
            $this->db->select($select_field);
        } else {
            $this->db->select('*');
        }

        if(!empty($conditions)) {
            foreach ($conditions as $key => $value) {
                $this->db->where($key, $value);
            }
        }

        $this->db->where('disable', 0);

        if(!empty($order_by)) {
            $this->db->order_by('id', $order_by);
        }

        if($result = $this->db->get($table)) {
            $result =  $result->result_array();
            if(isset($result[0])) {
                return ($return_array == true) ? $result : $result[0];
            }
        }
        return false;
    }

    /**
     * @param null $date_format
     * @return false|string
     * @author chieng_minh
     * @since 2019-11-06
     */
    public function _db_now($date_format = null) {

        static $database_now = '';

        if ($database_now == '') {
            $database_now = $this->db->query('SELECT NOW() AS now', false)->row()->now;
        }

        return ($date_format == null) ? $database_now : date($date_format, strtotime($database_now));
    }

    /**
     * @author chieng_minh
     * @since 2019-11-06
     * [CRE][49728][51918]
     */
    protected function _get_user_id() {

        $this->load->library('auth');

        static $user_id = null;

        if($user_id ===  null) {
            $user_id = $this->auth->get_user_id();
        }

        return $user_id;
    }
}