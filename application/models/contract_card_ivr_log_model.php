<?php

class Contract_card_ivr_log_model extends MC_Model {


    public function get_all_data() {
        return $this->_select('stub_ivr_contract_card_log','','', 'desc', true);
    }

    public function get_data_by_receipt_no($receipt_no = '') {

        if(!$receipt_no) {
            return false;
        }

        $sql = sprintf(<<<EOF
        SELECT 
                stub_ivr_contract_card_log.id as contract_card_ivr_log_id,
                stub_ivr_contract_card_log.discernment_id,
                stub_ivr_contract_card_log.customer_id,
                stub_ivr_contract_card_log.status_code,
                stub_ivr_contract_card_log.customer_phone,
                stub_ivr_contract_card_log.save_success_flag,
                stub_ivr_contract_card_log.receipt_no,
                stub_ivr_contract_card_log.gateway_type,
                stub_ivr_contract_card_log.create_datetime,            
                stub_ivr_pw_gmo_response.id as pw_gmo_response_id,
                stub_ivr_pw_gmo_response.gmo_member_id,
                stub_ivr_pw_gmo_response.gmo_member_name,
                stub_ivr_pw_gmo_response.seq_mode,
                stub_ivr_pw_gmo_response.card_seq,
                stub_ivr_pw_gmo_response.card_name,
                stub_ivr_pw_gmo_response.holder_name,
                stub_ivr_pw_gmo_response.is_member
            FROM stub_ivr_contract_card_log
            INNER JOIN stub_ivr_pw_gmo_response ON stub_ivr_pw_gmo_response.contract_card_ivr_log_id = stub_ivr_contract_card_log.id AND stub_ivr_pw_gmo_response.disable = 0
            WHERE stub_ivr_contract_card_log.receipt_no = '%1\$s'  AND stub_ivr_contract_card_log.disable = 0
EOF
            ,$receipt_no
        );

        $query = $this->db->query($sql);

        if($query) {

            $result_array =  $query->result_array();

            if(!empty($result_array[0])) {
                return $result_array[0];
            }
        }

        return false;
    }

    public function get_contract_call_data($contract_card_ivr_log_id = '') {

        if(!$contract_card_ivr_log_id) {
            return false;
        }

        $sql = sprintf(<<<EOF
        SELECT 
                stub_ivr_contract_card_log.id as contract_card_ivr_log_id,
                stub_ivr_contract_card_log.save_success_flag,
                stub_ivr_pw_gmo_response.id as pw_gmo_response_id,
                stub_ivr_pw_gmo_response.gmo_member_id,
                stub_ivr_pw_gmo_response.gmo_member_name,
                stub_ivr_pw_gmo_response.seq_mode,
                stub_ivr_pw_gmo_response.card_seq,
                stub_ivr_pw_gmo_response.card_name,
                stub_ivr_pw_gmo_response.holder_name,
                stub_ivr_pw_gmo_response.is_member,                
                gmo_setting.id as gmo_setting_id,
                gmo_setting.site_id,
                gmo_setting.site_password,
                gmo_setting.authorization_shop_id,
                gmo_setting.authorization_shop_password,
                gmo_setting.charge_shop_id,
                gmo_setting.charge_shop_password
            FROM stub_ivr_contract_card_log
            INNER JOIN stub_ivr_pw_gmo_response ON stub_ivr_pw_gmo_response.contract_card_ivr_log_id = stub_ivr_contract_card_log.id
            INNER JOIN gmo_setting ON gmo_setting.id = stub_ivr_pw_gmo_response.gmo_setting_id AND gmo_setting.disable = 0
            WHERE stub_ivr_contract_card_log.id = %1\$s 
EOF
            ,$contract_card_ivr_log_id
        );

        $query = $this->db->query($sql);

        if($query) {
            return $query->result_array();
        }

        return false;
    }

}