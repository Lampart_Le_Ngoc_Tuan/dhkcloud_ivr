<?php


class Common_model extends MC_Model {

    public function __construct()
    {
        parent::__construct();
    }


    /**
     * @param string $table
     * @param array $params
     * @return bool
     * @author chieng_minh
     * @since 2019-11-06
     */
    public function _insert_data($table = '', $params = array()) {

        if(!$table) {
            return false;
        }

        if(!$params) {
            return false;
        }

        return $this->_insert($table, $params);
    }

    /**
     * @param string $table
     * @param array $conditions
     * @param array $params
     * @return bool
     * @author chieng_minh
     * @since 2019-11-06
     * [CRE][49728][51918]
     */
    public function _update_data($table = '', $conditions = array(), $params = array()) {
        if(!$table || !$params || !$conditions) {
            return false;
        }

        return $this->_update($table, $conditions,  $params);
    }

    /**
     * @param null $date_format
     * @return false|string
     * @author chieng_minh
     * @since 2019-11-06
     * [CRE][49728][51918]
     */
    public function _db_now($date_format = null) {

        static $database_now = '';

        if ($database_now == '') {
            $database_now = $this->db->query('SELECT NOW() AS now', false)->row()->now;
        }

        return ($date_format == null) ? $database_now : date($date_format, strtotime($database_now));
    }

    /**
     * @param string $table
     * @param string $conditions
     * @param string $select_field
     * @return bool
     * @author chieng_minh
     * @since 2019-11-06
     * [CRE][49728][51918]
     */
    public function _select_data($table = '',$conditions = '', $select_field = '') {
        if(!$table) {
            return false;
        }

        return $this->_select($table, $conditions, $select_field);
    }
}