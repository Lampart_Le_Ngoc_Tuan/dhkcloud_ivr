<?php


class Gmo_setting_model extends MC_Model {

    public function __construct()
    {

    }

    public function select($conditions, $select_field = '') {

        if(empty($conditions)) {
            return false;
        }

        return $this->_select('gmo_setting', $conditions, $select_field);
    }
}