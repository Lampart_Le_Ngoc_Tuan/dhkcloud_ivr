<?php


class Ivr_model extends MC_Model {

    public $_customer_id              = '';
    public $_customer_phone           = '';
    public $_environment              = '';
    public $_card_no                  = '';
    public $_card_expired_month       = '';
    public $_card_expired_year        = '';
    public $_holder_name              = '';
    public $_security_code            = '';
    public $_action                   = '';
    public $_authorization_shop_id    = '';
    public $_pw_response_id           = '';
    public $_gmo_member_id            = '';
    public $_result_code_gmo_token    = '';
    public $_gmo_token                = '';
    public $_site_id                  = '';
    public $_card_name                = '';
    public $_update_type              = '';
    public $_site_password            = '';
    public $_card_seq                 = '';
    public $_seq_mode                 = 1;
    public $_default_flag             = '';
    public $_contract_card_ivr_log_id = '';
    const PW_CGI_URI_PG_LOCAL            = 'https://dev.lampart.com.vn/';
    const PW_CGI_URI_DEV                 = 'https://dev-pw-jp.fraise.jp/';
    const PW_CGI_URI_DEBUG_1             = 'https://debug-1-pw-jp.fraise.jp/';
    const PW_CGI_URI_DEBUG_2             = 'https://debug-2-pw-jp.fraise.jp/';
    const PW_CGI_URI_DEBUG_3             = 'https://debug-3-pw-jp.fraise.jp/';
    const PW_CGI_URI_PRE                 = 'https://pre-pw-jp.fraise.jp/';
    const PW_IVR_RESULT_CODE_SUCCESS     = 1;
    const PW_IVR_RESULT_CODE_FAIL        = 0;
    const PW_IVR_RESULT_CODE_SYSTEM_FAIL = 9;
    const PW_PSP_DIVISION_TYPE_PAYGENT   = 1;
    const PW_PSP_DIVISION_TYPE_GMO       = 2;
    const GMO_MEMBER_ID_PREFIX           = 'CCID';
    const ERROR_CODE_DUPLICATE_MEMBER    = 'E01';
    const ERROR_DETAIL_DUPLICATE_MEMBER  = 'E01390010';
    const SAVE_SUCCESS_FLG_CARD_TRUE     = 1;
    const SAVE_SUCCESS_FLG_CARD_FAIL     = 0;

	public function __construct() {
	    $this->set_value();

	}

	public function set_value() {
        if($_POST) {
            foreach ($_POST as $key => $value) {
                $this->{"_$key"} = $value;
            }
        }
    }

    public function request_to_pw() {

        $this->load->helper('json');
        $this->load->model('common_model');
        $_post_data = $get_pw_customer_info = array();
        try {
            if(!$this->_customer_phone || !$this->_customer_id) {
                throw new Exception('Customer phone and Customer id is required');
            }
            $this->_customer_phone = str_replace('-', '', trim($this->_customer_phone));
            $this->_customer_phone = str_replace(' ', '', trim($this->_customer_phone));

            if(!preg_match('/[0-9]/', $this->_customer_phone)) {
                throw new Exception('Customer phone only allow number', 404);
            }

            if(!preg_match('/[0-9]/', $this->_customer_id)) {
                throw new Exception('Customer id only allow number', 404);
            }

            $receipt_no    = $this->_generate_receipt_no('PW');

            $customer_data = array(
                'customer_id'    => $this->_customer_id,
                'customer_phone' => $this->_customer_phone,
            );

            $_post_data = array(
                'receiptNo'  => $receipt_no,
                'customerId' => isset($customer_data['customer_id']) ? $customer_data['customer_id'] : '',
                'telNo'      => isset($customer_data['customer_phone']) ? $customer_data['customer_phone'] : '',
            );

            $discernment_id       = 0;
            $request_result       = array();
            $get_pw_customer_info = array();

            switch ($this->_environment) {
                case 'dev':
                    $pwater_cgi_uri = self::PW_CGI_URI_DEV;
                    break;
                case 'debug_1':
                    $pwater_cgi_uri = self::PW_CGI_URI_DEBUG_1;
                    break;
                case 'debug_2':
                    $pwater_cgi_uri = self::PW_CGI_URI_DEBUG_2;
                    break;
                case 'debug_3':
                    $pwater_cgi_uri = self::PW_CGI_URI_DEBUG_3;
                    break;
                case 'pre':
                    $pwater_cgi_uri = self::PW_CGI_URI_PRE;
                    break;
                default:
                    // PG local environment
                    $pwater_cgi_uri = self::PW_CGI_URI_PG_LOCAL;
            }

            $pwater_cgi_uri = $pwater_cgi_uri . '_cgi/ivr_process/validate_customer_info';

            $get_pw_customer_info = request_json($pwater_cgi_uri, $_post_data);

            switch ($get_pw_customer_info['httpcode']) {
                case 200:
                    // xu ly update thong tin
                    $get_pw_customer_info['resultCode'] = isset($get_pw_customer_info['resultCode']) ? (string)$get_pw_customer_info['resultCode'] : 0;

                    // PW system fail
                    if ($get_pw_customer_info['resultCode'] == self::PW_IVR_RESULT_CODE_SYSTEM_FAIL) {
                        throw new Exception('PW system fail', 200);
                    }

                    // PW customer not found
                    if ($get_pw_customer_info['resultCode'] == self::PW_IVR_RESULT_CODE_FAIL) {
                        throw new Exception('Cannot found customer info in PW system', 200);
                    }

                    // Get PW customer info success
                    if ($get_pw_customer_info['resultCode'] == self::PW_IVR_RESULT_CODE_SUCCESS) {

                        // Insert customer info to db
                        $contract_card_ivr_log_params = array(
                            'discernment_id'    => 0,
                            'customer_id'       => $customer_data['customer_id'],
                            'customer_phone'    => $customer_data['customer_phone'],
                            'status_code'       => '',
                            'save_success_flag' => '',
                            'gateway_type'      => ($get_pw_customer_info['pspDivision'] == self::PW_PSP_DIVISION_TYPE_PAYGENT) ? 1 : 2,
                            'receipt_no'        => $receipt_no,
                        );

                        if(!$contract_card_ivr_log_id = $this->common_model->_insert_data('stub_ivr_contract_card_log', $contract_card_ivr_log_params)) {
                            throw new Exception('Cannot insert stub_ivr_contract_card_log');
                        }

                        if ($get_pw_customer_info['pspDivision'] == self::PW_PSP_DIVISION_TYPE_GMO) {
                            $gmo_setting_id = $this->common_model->_select_data(
                                'gmo_setting',
                                array('site_id' =>  $get_pw_customer_info['gmoSiteId'])

                            );

                            // Process GMO
                            $pw_gmo_data = array(
                                'gmo_setting_id'           => (!empty($gmo_setting_id)) ? $gmo_setting_id['id'] : '0',
                                'contract_card_ivr_log_id' => $contract_card_ivr_log_id,
                                'gmo_member_id'            => $get_pw_customer_info['gmoMemberId'],
                                'card_seq'                 => $get_pw_customer_info['cardSeq'],
                                'seq_mode'                 => $get_pw_customer_info['seqMode'],
                                'card_name'                => $get_pw_customer_info['cardName'],
                                'holder_name'              => $get_pw_customer_info['holderName'],
                                'is_member'                => $get_pw_customer_info['isMember'],
                            );

                            if(!$pw_response_id = $this->common_model->_insert_data('stub_ivr_pw_gmo_response', $pw_gmo_data)) {
                                throw new Exception('Cannot insert data to stub_ivr_pw_gmo_response');
                            }

                            $request_result['params']['pw_response_id']           = $pw_response_id;
                            $request_result['params']['contract_card_ivr_log_id'] = $contract_card_ivr_log_id;
                            $request_result['params']['gmo_member_id']            = $get_pw_customer_info['gmoMemberId'];

                        }

                        $request_result['result']             = true;
                        $request_result['err_message']        = 'Get data from PW success';
                        $request_result['params']['action']   = 'request';
                        $request_result['params']['request']  = $_post_data;
                        $request_result['params']['response'] = $get_pw_customer_info;
                    }

                    break;
                case 401:
                    throw new Exception('You do not have permission for this action');
                    break;
                default:
                    // httpcode = 0;

            } // End switch case

        } catch (Exception $exception) {

            if($exception->getCode() == 404) {
                return array(
                    'result'      => false,
                    'err_message' => $exception->getMessage(),
                );
            } else {
                return array(
                    'result'      => false,
                    'err_message' => $exception->getMessage(),
                    'params'      => array(
                        'request'  => $_post_data,
                        'response' => $get_pw_customer_info
                    ),
                );
            }
        }

        return $request_result;
    }

    public function request_to_gmo($data = array()) {

        $this->load->model('gmo_setting_model');

        try {

            if(empty($data)) {
                throw new Exception('');
            }

            foreach ($data as $key => $value) {
                $this->{"_$key"} = $value;
            }
            // Connect to gmo, register member_id and
            $this->load->library('gmo_payment_gateway/Pw_gmo_gateway');

            $params_member_info = array(
                'member_id'   => $data['gmo_member_id'],
                'member_name' => '',
                'site_id'     => $this->_site_id,
                'site_pass'   => $this->_site_password,
            );

            $member_info = $this->pw_gmo_gateway->register_member_id($params_member_info);
            file_put_contents(APPPATH . '/logs/params_member_info.txt', json_encode($params_member_info), FILE_APPEND | LOCK_EX);
            if(!empty($member_info)
               && ((isset($member_info['error_code'])
                    && $member_info['error_code'] == self::ERROR_CODE_DUPLICATE_MEMBER
                    && $member_info['error_info'] == self::ERROR_DETAIL_DUPLICATE_MEMBER)
                   || isset($member_info['member_id']))
            ) {
                $member_id = isset($member_info['member_id']) ? $member_info['member_id'] : $this->_gmo_member_id;

                $contract_gmo_params = array(
                    'member_id'                => $member_id,
                    'contract_card_ivr_log_id' => $data['contract_card_ivr_log_id'],
                    'error_code'               => isset($member_info['error_code']) ? $member_info['error_code'] : '',
                    'error_info'               => isset($member_info['error_info']) ? $member_info['error_info'] : '',
                );

                if(!$contract_gmo_id = $this->common_model->_insert_data('stub_ivr_gmo_member_info', $contract_gmo_params)) {
                    throw new Exception('Cannot insert stub_ivr_gmo_member_info');
                }

                $_params_card_info = array(
                    'member_id'     =>  $member_id,
                    'site_id'       =>  $this->_site_id,
                    'site_pass'     =>  $this->_site_password,
                    'seq_mode'      =>  $this->_seq_mode,
                    'card_seq'      =>  $this->_card_seq,
                    'default_flag'  =>  $this->_default_flag,
                    'card_name'     =>  $this->_card_name,
                    'token'         =>  $this->_gmo_token,
                    'update_type'   =>  $this->_update_type,
                );

                $card_info = $this->pw_gmo_gateway->register_card_id($_params_card_info);

                $contract_card_conditions = array(
                    'id' => $this->_contract_card_ivr_log_id,
                );

                if(!empty($card_info)
                   && !isset($card_info['error_code'])
                   && !isset($card_info['error_info'])
                   && isset($card_info['card_seq'])
                ) {

                    $gmo_card_info = array(
                        'contract_gmo_id'          => $contract_gmo_id,
                        'card_seq'                 => $card_info['card_seq'],
                        'contract_card_ivr_log_id' => $this->_contract_card_ivr_log_id,
                        'card_status'              => self::SAVE_SUCCESS_FLG_CARD_TRUE,
                    );

                    if(!$this->_insert('stub_ivr_gmo_card_info', $gmo_card_info)) {
                        throw new Exception('Cannot insert stub_ivr_gmo_card_info');
                    }

                    $contract_card_params = array(
                        'save_success_flag' => self::SAVE_SUCCESS_FLG_CARD_TRUE,
                    );

                    $this->_update('stub_ivr_contract_card_log', $contract_card_conditions, $contract_card_params);

                    return true;

                } else {
                    $gmo_card_info = array(
                        'contract_gmo_id'          => $contract_gmo_id,
                        'card_seq'                 => '',
                        'contract_card_ivr_log_id' => $this->_contract_card_ivr_log_id,
                        'card_status'              => self::SAVE_SUCCESS_FLG_CARD_FAIL,
                        'error_code'               => $card_info['error_code'],
                        'error_info'               => $card_info['error_info'],
                    );

                    if(!$this->_insert('stub_ivr_gmo_card_info', $gmo_card_info)) {
                        throw new Exception('Cannot insert stub_ivr_gmo_card_info');
                    }

                    $contract_card_params = array(
                        'save_success_flag' => self::SAVE_SUCCESS_FLG_CARD_FAIL,
                    );

                    $this->_update('stub_ivr_contract_card_log', $contract_card_conditions, $contract_card_params);

                    return false;
                }

            }

        } catch (Exception $exception) {

            $contract_card_conditions = array(
                'id' => $this->_contract_card_ivr_log_id,
            );

            $contract_card_params = array(
                'save_success_flag' => self::SAVE_SUCCESS_FLG_CARD_FAIL,
            );

            $this->_update('stub_ivr_contract_card_log', $contract_card_conditions, $contract_card_params);

            return false;
        }

        return false;
    }



    public function _generate_receipt_no($prefix = '') {

        if(strlen($prefix) > 2) {
            return false;
        }

        $date = date('Ymd');

        $conditions = array(
            "receipt_no like " => $prefix . $date . "%"
        );

        $count_receipt_no = 0;

        if($result_count = $this->_select('stub_ivr_contract_card_log', $conditions, '','', true)) {
            $count_receipt_no = count($result_count);
        }

        $receipt_no = $date . str_pad(++$count_receipt_no, 10, 0, STR_PAD_LEFT);

        if($prefix) {
            $receipt_no = $prefix . $receipt_no;
        }

        return $receipt_no;
    }
}