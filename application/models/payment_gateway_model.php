<?php


class Payment_gateway_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function get_customer_data_by_pw_gmo_response($pw_gmo_response = '') {

        if(!$pw_gmo_response) {
            return false;
        }

        $sql = sprintf(<<<EOF
            SELECT 
              call_log.id as call_log_id, 
              call_log.customer_id, 
              call_log.call_no, 
              pw_gmo_response.*, 
              gmo_setting.id as gmo_setting_id, 
              gmo_setting.site_id, 
              gmo_setting.site_password, 
              gmo_setting.authorization_shop_id, 
              gmo_setting.authorization_shop_password
            FROM call_log
            INNER JOIN pw_gmo_response ON pw_gmo_response.call_log_id = call_log.id AND pw_gmo_response.disabled = 0
            INNER JOIN gmo_setting ON pw_gmo_response.gmo_setting_id = gmo_setting.id AND gmo_setting.disabled = 0
            WHERE call_log.disabled = 0 AND pw_gmo_response.id = %1\$s            
            ORDER BY call_log.create_datetime DESC;
EOF
        , $pw_gmo_response
        );
        $query = $this->db->query($sql);

        if($query){
            return $query->result_array();
        }
    }
}