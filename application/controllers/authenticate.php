<?php


class Authenticate extends MC_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function login() {

        $this->load->library('auth');
        $view_assign = array();
        if(!$this->auth->authenticate()) {
            $username = (isset($_POST['username'])) ? $_POST['username'] : '';
            $password = (isset($_POST['password'])) ? $_POST['password'] : '';
            if($username || $password)  {
                if($this->auth->login($username, $password)) {
                    redirect('/ivr/index');
                } else {
                    $view_assign['data']['result'] = false;
                    $view_assign['data']['err_message'] = 'Username/password is invalid';
                }
            }
        } else {
            redirect('/ivr/index');
        }

        $view_assign['subview'] = $this->_controller . '/' . $this->_function;

        $this->load->view($this->_layout, $view_assign);
    }

    public function logout() {
        $this->load->library('auth');
        $this->auth->logout();
        redirect('/authenticate/login');
    }

}