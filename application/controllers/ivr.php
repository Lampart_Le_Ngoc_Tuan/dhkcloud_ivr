<?php 

class Ivr extends MC_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function index() {
        $this->load->model('contract_card_ivr_log_model');

        $view_assign['subview'] = $this->_controller . '/' . $this->_function;

        if($get_pw_customer_call_log = $this->contract_card_ivr_log_model->get_all_data()) {
            $view_assign['get_pw_customer_call_log'] = isset($get_pw_customer_call_log) ? $get_pw_customer_call_log : 0;
        }

        $this->load->view($this->_layout, $view_assign);
	}

	public function get_customer_info()
    {
        $this->load->model('ivr_model');

        if($this->ivr_model->_customer_id && $this->ivr_model->_customer_phone) {

            $form = $this->ivr_model->request_to_pw();

            if(isset($form['result']) && $form['result']) {
                $view_assign['params']['result']      = $form['result'];
                $view_assign['params']['err_message'] = $form['err_message'];
                $view_assign['params']['request']     = isset($form['params']['request']) ? $form['params']['request'] : '';
                $view_assign['params']['response']    = isset($form['params']['response']) ? $form['params']['response'] : '';
                if ($form['result'] === true) {
                    $this->load->model('gmo_setting_model');

                    if (!empty($form['params']['response']['gmoSiteId'])) {
                        $conditions = array(
                            'site_id' => $form['params']['response']['gmoSiteId'],
                        );
                        if ($gmo_setting = $this->gmo_setting_model->select($conditions)) {
                            $view_assign['params']['authorization_shop_id'] = $gmo_setting['authorization_shop_id'];
                        }
                        $view_assign['params']['contract_card_ivr_log_id'] = isset($form['params']['contract_card_ivr_log_id'])
                            ? $form['params']['contract_card_ivr_log_id'] : '';
                    }
                    $view_assign['action'] = $form['params']['action'];

                }
            } else {
                if(isset($form['err_message'])) {
                    $this->session->set_flashdata('error', $form['err_message']);
                }
            }
        }

        $view_assign['subview'] = $this->_controller . '/' . $this->_function;

        $this->load->view($this->_layout, $view_assign);
    }

    public function connect_to_gmo() {

        $this->load->model('ivr_model');
        $this->load->model('contract_card_ivr_log_model');
        $this->load->model('common_model');

        if(!$log_id = $_REQUEST['log_id']) {
            redirect('/ivr/index');
        }

        $contract_data = $this->contract_card_ivr_log_model->get_contract_call_data($log_id);
        $contract_data = $contract_data[0];

        if(!empty($contract_data['save_success_flag'])) {
            $this->session->set_flashdata('error', 'This card info is exist in GMO system');
            redirect('/ivr/index');
        }

        $view_assign['params']['authorization_shop_id'] = $contract_data['authorization_shop_id'];

        $view_assign['subview'] = $this->_controller . '/' . $this->_function;

        $this->load->view($this->_layout, $view_assign);

        if($card_data = $_POST) {

            if($card_data['action'] != 'send_card_to_gmo') {
                return false;
            }

            if($card_data['result_code_gmo_token'] != '000') {
                return false;
            }

            if($request_to_gmo = $this->ivr_model->request_to_gmo($contract_data)) {
                $this->session->set_flashdata('success', 'This card is valid');
            } else {
                $this->session->set_flashdata('info', 'This card is invalid');
            }

            redirect('ivr/index');
        }
    }

    public function connect_to_paygent() {
        $view_assign['subview'] = $this->_controller . '/' . $this->_function;

        $this->load->view($this->_layout, $view_assign);
    }

    public function _validate_ivr_params($params = array(), $action = '') {

        try {
            switch ($action) {
                case 'request':

                    if(empty($params['customer_id']) || empty($params['customer_phone'])) {
                        throw new Exception('Empty customer_id or phone');
                    }

                    if(preg_match('/[^0-9-]/i', $params['customer_id'])) {
                        throw new Exception('Customer id is invalid: ' . htmlentities($params['customer_id']));
                    }

                    if(preg_match('/[^0-9]/i', $params['customer_phone'])) {
                        throw new Exception('Phone number invalid: ' . htmlentities($params['customer_phone']));
                    }

                    $replace_pattern = '/[-]/i';

                    $params['customer_phone'] = preg_replace($replace_pattern, '', trim($params['customer_phone']));
                    break;

                case 'response':
                    break;
            }
        } catch (Exception $exception) {

            return array(
                'result'      => false,
                'err_line'    => $exception->getLine(),
                'err_file'    => __FILE__,
                'err_message' => $exception->getMessage(),
            );
        }

        return array(
            'result' => true,
            'data'   => $params
        );
    }
}