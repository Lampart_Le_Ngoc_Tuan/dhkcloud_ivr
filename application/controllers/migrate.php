<?php  if ( ! defined('BASEPATH')) exit("No direct script access allowed");

class Migrate extends MC_Controller {
    public function __construct()
    {
        parent::__construct();

        $this->load->library('migration');
    }

    public function index() {
        $is_migrate_all = config_item('is_migrate_all');
        $this->version('1', $is_migrate_all);
    }

    public function _find_migration() {
        $result         = array();
        $migration_path = APPPATH . 'migrations/';

        if(is_dir($migration_path)) {
            if($handle = opendir($migration_path)) {
                while(($file = readdir($handle)) != false) {
                    if(($path_info = pathinfo($file,PATHINFO_EXTENSION)) == 'php') {
                        $result[] = $file;
                    }
                }
            }
        }
        return $result;
    }

    public function version($version, $migrate_all = false)
    {
        try {
            if($migrate_all) {
                if($migration_files = $this->_find_migration()) {
                    foreach ($migration_files as $migration) {
                        $version = substr($migration, 2,1);
                        if(!$this->migration->version($version)) {
                            throw new Exception($this->migration->error_string());
                        }
                        echo "<p>Migration files({$migration}) success!!!</p>";
                    }
                }
            } else {
                if(!$this->migration->version($version)) {
                    throw new Exception($this->migration->error_string());
                }
                echo 'Migration files(' . $version . ') success' . "\n";
            }
        } catch (Exception $exception) {
            echo $exception->getMessage();
        }
        return;
    }
}