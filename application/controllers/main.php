<?php

class main extends MC_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->set_layout();
    }

    public function set_layout(){
        $data['subview'] = $this->_controller . '/' . $this->_function;
        $this->load->view($this->_layout, $data);
    }

    public function index() {

    }
}