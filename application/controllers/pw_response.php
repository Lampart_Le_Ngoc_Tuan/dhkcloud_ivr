<?php


class Pw_response  extends MC_Controller {


    public $_apiKey          = '';
    public $_apiPassword     = '';
    public $_detailReturnFlg = '';
    public $_receiptNo       = '';

    public function __construct()
    {
        parent::__construct();
    }


    public function AuthSearchApi() {

        $this->load->model('contract_card_ivr_log_model');
        $this->load->model('common_model');

        $data_response = array();

        try {
            if(strtolower($this->input->server('REQUEST_METHOD')) != 'post') {
                throw new Exception("The requested resource does not support http method 'GET'.", 400);
            }

            $input = file_get_contents('php://input');

            $data_request = json_decode($input,true);

            if(count($data_request) < 3 || count($data_request) > 5) {
                throw new Exception('Request parameter not match', 30);
            }

            if(!isset($data_request['apiKey'])) {
                throw new Exception('apiKey parameter is required', 30);
            }

            if(!isset($data_request['apiPassword'])) {
                throw new Exception('apiPassword parameter is required', 30);
            }

            if(empty($data_request['receiptNo'])) {
                throw new Exception('receiptNo parameter is required', 30);
            }

            foreach ($data_request as $key => $value) {
                $this->{"_$key"} = $value;
            }

            $save_card = $save_member = $data_request = array();

            if(!$contract_card_info = $this->contract_card_ivr_log_model->get_data_by_receipt_no($this->_receiptNo)) {
                throw new Exception('Another error', 40);
            }

            $data_response = array(
                'resultCode'               => 0,
                'discernmentId'            => $contract_card_info['receipt_no'],// receiptNo,
                'discernmentIdEndDatetime' => $contract_card_info['create_datetime'],
                'statusCode'               => '',
                'flowLevelId'              => '',
                'flowLevelName'            => '',
                'callerTelNo'              => $contract_card_info['customer_phone'], // So dt nguoi goi
                'incomingDatetime'         => $contract_card_info['create_datetime'], // Call time (create_datetime)
                'anyItem'                  => array(
                    'name'  => '',
                    'value' => '',
                ),
            );

            if($contract_card_info['gateway_type'] == 2 && $contract_card_info['save_success_flag'] != '') {

                if (!$stub_ivr_gmo_member_info = $this->common_model->_select_data('stub_ivr_gmo_member_info', array(
                    'contract_card_ivr_log_id' => $contract_card_info['contract_card_ivr_log_id'],
                    'disable'                 => 0
                ))) {
                        return false;
                }

                $save_member = array(
                    'pspStatusCode'       => 1,
                    'transactionDatetime' => $stub_ivr_gmo_member_info['create_datetime'],
                    'memberId'            => $contract_card_info['gmo_member_id'],
                    'memberId'            => $contract_card_info['gmo_member_id'],
                    'errCode'             => $stub_ivr_gmo_member_info['error_code'],
                    'errInfo'             => $stub_ivr_gmo_member_info['error_info'],
                );

                if(!$gmo_card_info = $this->common_model->_select_data('stub_ivr_gmo_card_info', array(
                    'contract_card_ivr_log_id' => $contract_card_info['contract_card_ivr_log_id'],
                    'disable'                 => 0
                ))) {
                    return false;
                }

                $save_card = array(
                    'pspStatusCode'          => 0,
                    'transactionDatetime'    => $gmo_card_info['create_datetime'],
                    'cardSeq'                => $gmo_card_info['card_seq'],
                    'cardNo'                 => '',
                    'forward'                => '',
                    'errCode'                => $gmo_card_info['error_code'],
                    'errInfo'                => $gmo_card_info['error_info'],
                    'brand'                  => '',
                    'domesticFlag'           => '',
                    'issuerCode'             => '',
                    'debitPrepaidFlag'       => '',
                    'debitPrepaidIssuerName' => '',
                    'forwardFinal'           => '',
                );
            }

            if($contract_card_info['save_success_flag'] != '') {
                $data_response = array_merge($data_response, array('saveSuccessFlg' => $contract_card_info['save_success_flag']));
            }

            /**
             * Phần này chưa viết nàk
             */
            if(isset($contract_card_info['gateway_type']) && $contract_card_info['gateway_type'] == 1) {
                $data_response .= array(
                    'lastTransaction' => array(
                        'telegram025' => array(),
                    )
                );
            }

            if(isset($contract_card_info['gateway_type']) && $contract_card_info['gateway_type'] == 2) {
                $transactionHistory =  array();

                $gmo_response = array(
                    'lastTransaction' => array(
                        'saveMember' => $save_member,
                        'saveCard' => $save_card,
                    )
                );

                if(isset($data_request['detailReturnFlg']) && $data_request['detailReturnFlg'] == 1) {
                    $transactionHistory = array(
                        'transactionHistory' => array(
                            'lastTransaction' => array(
                                'saveMember' => $save_member,
                                'saveCard'   => $save_card
                            )
                        ),
                    );
                }

                $data_response = array_merge($data_response, $gmo_response);
                $data_response = array_merge($data_response, $transactionHistory);
            }

        } catch(Exception $exception) {

            if($exception->getCode() == 30) {
                $data_response = array(
                    'resultCode'       => $exception->getCode(),
                    'errorMessageList' => $exception->getMessage(),
                );
            }

            if($exception->getCode() == 40) {
                $data_response = array(
                    'resultCode'       => $exception->getCode(),
                );
            }

            if($exception->getCode() == 90) {
                $data_response = array(
                    'resultCode'       => $exception->getCode(),
                    'errorMessageList' => $exception->getMessage(),
                );
            }

            if($exception->getCode() == 400) {
                $data_response = array('Message' => $exception->getMessage());
            }

        }

        echo json_encode($data_response);
        exit();

    }

    public function _validate_request_params($params = array()) {

    }
}