<?php

class post_controller_constructor {

    public $_ci;

    public function __construct()
    {
        $this->_ci              =& get_instance();
        $this->_ci->_controller = $this->_ci->router->fetch_class();
        $this->_ci->_function   = $this->_ci->router->fetch_method();
        $this->_ci->_load_view  = true;
        $this->init();
        $this->_do_authenticate();
    }

    public function init() {
        if(empty($this->_ci->_layout)) {
            $pathinfo = '';
            if(isset($_SERVER['PATH_INFO'])) {
                $pathinfo = trim(pathinfo($_SERVER['PATH_INFO'], PATHINFO_DIRNAME));
            }

            $dirname = str_replace('/','', $pathinfo);

            $this->_layout = 'layouts/default/main';

            if($dirname == 'admin') {
                $this->_layout = 'layouts/admin/main';
            }
        }
        $this->_ci->_layout = $this->_layout;
    }

    public function _do_authenticate() {
        $this->_ci->load->library('auth');
        $this->_ci->load->helper('url');

        if(!$this->_ci->auth->authenticate()) {
            if($this->_ci->_controller != 'authenticate' && $this->_ci->_function != 'login') {
                $migration = config_item('is_deploy');
                if(!$migration) {
                    redirect('/authenticate/login');
                }
            }
        }
    }
}