<form method="POST">
    <div class="form-group">
        <?php if(isset($data['err_message']) && $data['result'] === false): ?>
            <div class="alert alert-danger"><?php echo $data['err_message'] ?> </div>
        <?php endif ?>
    </div>
    <div class="form-group">
<!--        <label for="username">Username</label>-->
        <input type="text" name="username" id="username" placeholder="Username" class="form-control">
    </div>
    <div class="form-group">
<!--        <label for="password">Password</label>-->
        <input type="text" name="password" id="password" placeholder="Password" class="form-control">
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary btn-lg btn-block">
            <span class="glyphicon glyphicon-search" aria-hidden="true"></span> Login
        </button>
    </div>
</form>