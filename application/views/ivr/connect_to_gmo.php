<div class="row">
    <div class="col-md-6 offset-md-3">

        <div class="message"></div>
        <form method="POST">
            <div class="form-group">
                <label for="card_no">Credit Card Number</label>
                <input
                    type="text"
                    name="card_no"
                    id="card_no"
                    placeholder="Credit Card Number"
                    maxlength="16"
                    minlength="10"
                    class="form-control"
                    value="3540111111111111"
                >
            </div>
            <div class="form-group">
                <label for="card_expired">Card Valid Throw</label>
                <div class="row">
                    <div class="col-md-6">
                        <select name="card_expired_month" id="card_expired_month" class="form-control">
                            <option value="01">01</option>
                            <option value="02">02</option>
                            <option value="03">03</option>
                            <option value="04">04</option>
                            <option value="05">05</option>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <select name="card_expired_year" id="card_expired_year" class="form-control">
                            <option value="2019">2019</option>
                            <option value="2020" selected>2020</option>
                            <option value="2021">2021</option>
                            <option value="2022">2022</option>
                            <option value="2023">2023</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="holder_name">Holder Name</label>
                <input type="text" name="holder_name" id="holder_name" placeholder="Holder Name" maxlength="20" class="form-control" value="Minh Chieng">
            </div>
            <div class="form-group">
                <label for="security_code">CVV Number</label>
                <input type="text" name="security_code" id="security_code" placeholder="CVV Number" maxlength="4" class="form-control" value="1991">
            </div>

            <div class="form-group">
                <input type="hidden" name="action" value="send_card_to_gmo">
                <input
                    type="hidden"
                    name="authorization_shop_id"
                    value="<?php echo (isset($params['authorization_shop_id'])) ? $params['authorization_shop_id'] : '0' ?>">
                <button type="button" class="btn btn-primary btn-lg btn-block" id="get_gmo_token">SEND</button>
                <button type="submit" class="btn btn-primary btn-lg btn-block" id="send_to_gmo" style="display: none">SEND</button>
            </div>
        </form>
    </div>
</div>