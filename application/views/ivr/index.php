<a href="/ivr/get_customer_info" class="btn btn-success">Request Data From PW</a>
<br/><br/>
<?php
    if($this->session->flashdata('error')) {
        echo "<div class='alert alert-danger'>" . $this->session->flashdata('error') . "</div>";
    }

    if($this->session->flashdata('success')) {
        echo "<div class='alert alert-success'>" . $this->session->flashdata('success') . "</div>";
    }

    if($this->session->flashdata('info')) {
        echo "<div class='alert alert-info'>" . $this->session->flashdata('info') . "</div>";
    }
?>
<table class="table">
    <tr class="text-center">
        <th>ID</th>
        <th>Contract ID</th>
        <th>Status Code</th>
        <th>Receipt No</th>
        <th>Card Status</th>
        <th>Payment Gateway</th>
        <th>Action</th>
    </tr>

    <?php
        if(isset($get_pw_customer_call_log)) :
            foreach ($get_pw_customer_call_log as $data):
//var_dump($get_pw_customer_call_log);
    ?>
    <tr class="text-center">
        <td><?php echo $data['id'] ?></td>
        <td><?php echo $data['customer_id'] ?></td>
        <td>
            <?php
                $status_code_color        = 'bg-color-grey';
                $status_code_description = 'Customer not contact to GMO';
                $status_code = (string)$data['status_code'];
                switch ($status_code):
                    case '0':
                        $status_code_color        = 'bg-color-orange';
                        $status_code_description = 'Customer is not call';
                        break;
                    case '1':
                        $status_code_color        = 'bg-color-violet';
                        $status_code_description = 'Customer calling';
                        break;
                    case '2':
                        $status_code_color        = 'bg-color-green';
                        $status_code_description = 'Customer called';
                        break;
                    default:
                        $status_code_color        = 'bg-color-red';
                        $status_code_description  = 'Not use status code';
                endswitch;

                if($status_code == '') {
                    echo '-';
                }
                else {
            ?>
                <div class="text-center status <?php echo $status_code_color ?>" title = "<?php echo $status_code_description ?>"></div>
            <?php } ?>
        </td>
        <td><?php echo $data['receipt_no'] ?></td>
        <td>
            <?php

                $save_success_flag_color = 'bg-color-grey';
                $save_success_flag_alt   = 'Card not confirm';
                $save_success_flag = (string)$data['save_success_flag'];
                switch ($save_success_flag) {
                    case "1":
                        $save_success_flag_color = 'bg-color-green';
                        $save_success_flag_alt   = 'Card success';
                        break;
                    case "0":
                        $save_success_flag_color = 'bg-color-red';
                        $save_success_flag_alt   = 'Card invalid';
                        break;
                    default:
                        $save_success_flag_color = 'bg-color-grey';
                        $save_success_flag_alt   = 'Card not confirm';
                }
            ?>
            <div class="status <?php echo $save_success_flag_color ?>" title="<?php echo $save_success_flag_alt ?>"> </div>
        </td>
        <td><?php echo ($data['gateway_type'] == 1) ? 'Paygent' : 'GMO' ?></td>
        <td>
            <?php if('' == $data['save_success_flag']): ?>
                <?php if($data['gateway_type'] == 1): ?>
                    <a href="/ivr/connect_to_paygent" class="btn btn-success">Connect To Paygent</a>
                <?php elseif ($data['gateway_type'] == 2): ?>
                    <a href="/ivr/connect_to_gmo?log_id=<?php echo $data['id']; ?>" class="btn btn-primary">Connect To GMO</a>
                <?php endif ?>
            <?php endif ?>
        </td>
    </tr>
    <?php
            endforeach;
        endif;
    ?>
</table>
