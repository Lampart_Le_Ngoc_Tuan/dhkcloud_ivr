<div class="row">
    <div class="col-md-6">
        <?php

        if(!empty($params['request'])){
            echo "Request:";
            echo '<div class="alert alert-secondary"> <ul>';
            foreach ($params['request'] as $key => $value) {
                echo "<li>{$key}&nbsp;&nbsp;&nbsp;: {$value}</li>";
            }
            echo '</ul></div>';
        }

        if(!empty($params['response'])){
            echo "<hr/>■ Response:";
            echo '<div class="alert alert-secondary"> <ul>';
            foreach ($params['response'] as $key => $value) {
                echo "<li>{$key}&nbsp;&nbsp;&nbsp;: {$value}</li>";
            }
            echo '</ul></div>';
        }
        ?>
    </div>

    <div class="col-md-6">
        <?php
            if($this->session->flashdata('error')) {
                echo "<div class='alert alert-danger'>" . $this->session->flashdata('error') . "</div>";
            }

            if($this->session->flashdata('success')) {
                echo "<div class='alert alert-success'>" . $this->session->flashdata('success') . "</div>";
            }

            if($this->session->flashdata('info')) {
                echo "<div class='alert alert-info'>" . $this->session->flashdata('info') . "</div>";
            }
        ?>
        <?php if(isset($params['result']) && $params['err_message']) : ?>
            <div class="alert <?php echo ($params['result'] == true) ? 'alert-success' : 'alert-danger' ?>">
                <?php echo $params['err_message'] ?>
            </div>
        <?php endif?>
        <?php if(isset($action) && $action == 'request'): ?>

        <div class="message"></div>
        <form method="POST" action="/ivr/connect_to_gmo?log_id=<?php echo $params['contract_card_ivr_log_id'] ?>">
            <div class="form-group">
                <label for="card_no">Credit Card Number</label>
                <input
                        type="text"
                        name="card_no"
                        id="card_no"
                        placeholder="Credit Card Number"
                        maxlength="16"
                        minlength="10"
                        class="form-control"
                        value="3540111111111111"
                >
            </div>
            <div class="form-group">
                <label for="card_expired">Card Valid Throw</label>
                <div class="row">
                    <div class="col-md-6">
                        <select name="card_expired_month" id="card_expired_month" class="form-control">
                            <option value="01">01</option>
                            <option value="02">02</option>
                            <option value="03">03</option>
                            <option value="04">04</option>
                            <option value="05">05</option>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <select name="card_expired_year" id="card_expired_year" class="form-control">
                            <option value="2019">2019</option>
                            <option value="2020" selected>2020</option>
                            <option value="2021">2021</option>
                            <option value="2022">2022</option>
                            <option value="2023">2023</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="holder_name">Holder Name</label>
                <input type="text" name="holder_name" id="holder_name" placeholder="Holder Name" maxlength="20" class="form-control" value="">
            </div>
            <div class="form-group">
                <label for="security_code">CVV Number</label>
                <input type="text" name="security_code" id="security_code" placeholder="CVV Number" maxlength="4" class="form-control" value="">
            </div>

            <div class="form-group">
                <input type="hidden" name="action" value="send_card_to_gmo">
                <input
                        type="hidden"
                        name="authorization_shop_id"
                        value="<?php echo (isset($params['authorization_shop_id'])) ? $params['authorization_shop_id'] : '0' ?>">
                <button type="button" class="btn btn-primary btn-lg btn-block" id="get_gmo_token">SEND</button>
                <button type="submit" class="btn btn-primary btn-lg btn-block" id="send_to_gmo" style="display: none">SEND</button>
            </div>
        </form>
    <?php else: ?>
        <form method="POST">
            <div class="form-group">
                <label for="customer_id">Customer ID <small class="text-danger"><i>(Only allow number, example: 123456)</i></small></label>
                <input type="text" name="customer_id" id="customer_id" placeholder="Customer ID" class="form-control" value="1306451" maxlength="11">
            </div>
            <div class="form-group">
                <label for="customer_phone">Customer Phone Number <small class="text-danger"><i>(Only allow number and hyphen character (-), example: 123-416-142)</i></small></label>
                <input type="text" name="customer_phone" id="customer_phone" placeholder="Customer Phone Number" class="form-control" value="655-1111-3235" maxlength="16">
            </div>
            <div class="form-group">
                <label for="environment">ENVIRONMENT</label>
                <select name="environment" id="environment" class="form-control">
                    <option value="pg_local">PG LOCAL</option>
                    <option value="dev" selected>DEV</option>
                    <option value="debug_1">DEBUG 1</option>
                    <option value="debug_2">DEBUG 2</option>
                    <option value="debug_3">DEBUG 3</option>
                </select>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block btn-lg">SEND</button>
            </div>
        </form>
    <?php endif ?>
    </div>
</div>
