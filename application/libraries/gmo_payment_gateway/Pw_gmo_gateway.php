<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Libary for gmo payment method
 * Author: van_hieu
 * @since 2019-08-05
 * @task 49728
 */
Class Pw_gmo_gateway {

    public function __construct()
    {
        global $CI;
        $this->ci =& $CI;
    }
    /**
     * register_member_id
     *
     * @author ngoc_tuan
     * @param array $_params
     * @since 2019-08-05
     * @return array
     * [CRE][49728]
     */
    public function register_member_id($_params)
    {
        require_once('input/SaveMemberInput.php');
        require_once('tran/SaveMember.php');
        require_once('common/Gmopg_Log.php');

        $input = new SaveMemberInput();

        $input->setSiteId($_params['site_id']);

        $input->setSitePass($_params['site_pass']);

        $input->setMemberId($_params['member_id']);

        $input->setMemberName( mb_convert_encoding($_params['member_name'], 'SJIS', 'UTF-8' ));

        $_params['site_id']   = $input->getSiteId();
        $_params['site_pass'] = $input->getSitePass();
        $_params['register']  = 'REGISTER_MEMBER';

        $this->log = new Gmopg_Log(get_class($this));
        $this->log->className_ = 'Pw_gmo_gateway';

        $_params_log = array(
            'function'     => 'register_member_id',
            'data'         => $input->toString(),
            'data_request' => $_params,
        );

        $exe = new SaveMember();

        $output = $exe->exec($input);

        if(isset($output->map['errcode'])) {
            $_params_log['error'] = implode(" & ", $output->map);
            $this->log->error($_params_log);

            return array(
                'error_code' => $output->map['errcode'],
                'error_info' => $output->map['errinfo'],
            );
        } elseif($output->memberId != null) {
            $_params_log['result'] = $output->memberId;
            $this->log->info($_params_log);

            return array(
                'member_id' => $output->memberId,
            );
        }
    }
    /**
     * register_card_id
     *
     * @author ngoc_tuan
     * @param array $_params
     * @since 2019-08-05
     * @return array
     * [CRE][49728]
     */
    public function register_card_id($_params)
    {
        require_once('input/SaveCardInput.php');
        require_once('tran/SaveCard.php');
        require_once('common/Gmopg_Log.php');

        $input = new SaveCardInput();

        $input->setSiteId($_params['site_id']);

        $input->setSitePass($_params['site_pass']);

        $input->setMemberId($_params['member_id']);

        $input->setSeqMode($_params['seq_mode']);

        $input->setCardSeq($_params['card_seq']);

        $input->setDefaultFlag($_params['default_flag']);

        $input->setCardName(mb_convert_encoding($_params['card_name'] , 'SJIS' , 'UTF-8'));

        $input->setToken($_params['token']);

        $input->setUpdateType($_params['update_type']);

        $_params['site_id']   = $input->getSiteId();
        $_params['site_pass'] = $input->getSitePass();
        $_params['register']  = 'REGISTER_CARD';

        $this->log = new Gmopg_Log(get_class($this));
        $this->log->className_ = 'Pw_gmo_gateway';

        $_params_log = array(
            'function'     => 'register_card_id',
            'data'         => $input->toString(),
            'data_request' => $_params,
        );
        $exe = new SaveCard();

        $output = $exe->exec( $input );

        if(isset($output->map['errcode'])) {
            $_params_log['error'] = implode(" & ", $output->map);
            $this->log->error($_params_log);

            return array(
                'error_code' => $output->map['errcode'],
                'error_info' => $output->map['errinfo'],
            );
        } elseif($output->cardNo != null) {
            $_push_info = array();

            foreach ($output as $key => $value){
                if($key == 'errList') {
                    break;
                }

                $_push_info[$key] = $value;
            }

            $_params_log['result'] = $_push_info;
            $this->log->info($_params_log);

            return array(
                'card_seq' => $output->cardSeq,
            );
        }
    }
    /**
     * gmo_cancel_payment
     *
     * @author van_hieu
     * @param array $_params
     * @since 2019-10-02
     * @return array
     * [CRE][49728][54642]
     */
    public function gmo_cancel_payment($_params)
    {
        require_once('input/AlterTranInput.php');
        require_once('tran/AlterTran.php');
        require_once('common/Gmopg_Log.php');

        $input = new AlterTranInput();

        $input->setShopId($_params['shop_id']);
        $input->setShopPass($_params['shop_password']);
        $input->setAccessId($_params['payment_id']);
        $input->setAccessPass($_params['payment_password']);
        $input->setJobCd($_params['jobcd']);

        $_params['shop_id']   = $input->getShopId();
        $_params['shop_password'] = $input->getShopPass();
        $_params['register']  = 'CANCEL PAYMENT';

        $this->log = new Gmopg_Log(get_class($this));
        $this->log->className_ = 'Pw_gmo_gateway';

        $_params_log = array(
            'function'     => 'gmo_cancel_payment',
            'data'         => $input->toString(),
            'data_request' => $_params,
        );
        $exe = new AlterTran();

        $output = $exe->exec( $input );

        if(isset($output->map['errcode'])) {
            $_params_log['error'] = implode(" & ", $output->map);
            $this->log->error($_params_log);

            return array(
                'status'     => false,
                'error_code' => $output->map['errcode'],
                'error_info' => $output->map['errinfo'],
            );
        } else {
            $_push_info = array();

            foreach ($output as $key => $value){
                if($key == 'errList') {
                    break;
                }

                $_push_info[$key] = $value;
            }
            $_params_log['result'] = $_push_info;
            $this->log->info($_params_log);
            return array(
                'status'     => true,
                'response'    => $_params_log['result']
            );
        }
    }
    /**
     * change_money_charge
     *
     * @author ngoc_tuan
     * @param array $_params
     * @since 2019-10-03
     * @return array
     * [CRE][49728][54642]
     */
    public function change_money_charge($_params){
        require_once('input/ChangeTranInput.php');
        require_once('tran/ChangeTran.php');
        require_once('common/Gmopg_Log.php');

        $input = new ChangeTranInput();

        $input->setShopId($_params['shop_id']);
        $input->setShopPass($_params['shop_password']);
        $input->setAccessId($_params['payment_id']);
        $input->setAccessPass($_params['payment_password']);
        $input->setJobCd($_params['jobcd']);
        $input->setAmount($_params['amount']);
        $input->setTax($_params['tax']);

        $_params['shop_id']   = $input->getShopId();
        $_params['shop_password'] = $input->getShopPass();
        $_params['register']  = 'change_money_charge ';

        $this->log = new Gmopg_Log(get_class($this));
        $this->log->className_ = 'Pw_gmo_gateway';

        $_params_log = array(
            'function'     => 'change_money_charge',
            'data'         => $input->toString(),
            'data_request' => $_params,
        );
        $exe = new ChangeTran();

        $output = $exe->exec( $input );

        if(isset($output->map['errcode'])) {
            $_params_log['error'] = implode(" & ", $output->map);
            $this->log->error($_params_log);

            return array(
                'status'     => false,
                'error_code' => $output->map['errcode'],
                'error_info' => $output->map['errinfo'],
            );
        } else {
            $_push_info = array();

            $_push_info['accessId'] = $output->getAccessId();
            $_push_info['accessPass'] = $output->getAccessPass();
            $_push_info['forward'] = $output->getForward();
            $_push_info['approve'] = $output->getApprovalNo();
            $_push_info['tranDate'] = $output->getTranDate();
            $_push_info['tranId'] = $output->getTranId();

            $_params_log['result'] = $_push_info;
            $this->log->info($_params_log);
            return array(
                'status'     => true,
                'response'    => $_params_log['result']
            );
        }
    }

    /**
     * initialization_transaction_entry_service
     *
     * @author ngoc_tuan
     * @param array $_params
     * @since 2019-10-03
     * @return array
     * [CRE][49728][option]
     */
    public function initialization_transaction_entry_service($_params){
        require_once('input/EntryTranInput.php');
        require_once('tran/EntryTran.php');
        require_once('common/Gmopg_Log.php');

        $input = new EntryTranInput();
        $input->setShopId($_params['shop_id']);
        $input->setShopPass($_params['shop_password']);
        $input->setJobCd($_params['job_cd']);
        $input->setOrderId($_params['order_id']);
        $input->setAmount($_params['amount']);

        $_params['shop_id']   = $input->getShopId();
        $_params['shop_password'] = $input->getShopPass();
        $_params['register']  = 'initialization transaction';

        $this->log = new Gmopg_Log(get_class($this));
        $this->log->className_ = 'Pw_gmo_gateway';

        $_params_log = array(
            'function'     => 'initialization_transaction',
            'data'         => $input->toString(),
            'data_request' => $_params,
        );

        $exe = new EntryTran();
        $output = $exe->exec( $input );
        if(isset($output->map['errcode'])) {
            $_params_log['error'] = implode(" & ", $output->map);
            $this->log->error($_params_log);

            return array(
                'status'     => false,
                'error_code' => $output->map['errcode'],
                'error_info' => $output->map['errinfo'],
            );
        } else {
            $_push_info = array();

            $_push_info['accessId'] = $output->getAccessId();
            $_push_info['accessPass'] = $output->getAccessPass();

            $_params_log['result'] = $_push_info;
            $this->log->info($_params_log);
            return array(
                'data'      => $_push_info,
                'status'    => true,
            );
        }
    }

    /**
     * create_transaction_temporary
     *
     * @author ngoc_tuan
     * @param array $_params
     * @since 2019-10-03
     * @return array
     * [CRE][49728][option]
     */
    public function create_transaction_temporary($_params){
        require_once('input/ExecTranInput.php');
        require_once('tran/ExecTran.php');
        require_once('common/Gmopg_Log.php');

        $input = new ExecTranInput();
        $input->setSiteId($_params['site_id']);
        $input->setSitePass($_params['site_pass']);
        $input->setAccessId($_params['accessId']);
        $input->setAccessPass($_params['accessPass']);
        $input->setOrderId($_params['order_id']);
        $input->setMemberId($_params['member_id']);
        $input->setCardSeq($_params['card_seq']);
        $input->setMethod($_params['pay_method']);
        $input->setPayTimes($_params['pay_time']);
        $input->setClientField1(mb_convert_encoding( $_params['client_field_1'] , 'SJIS' , 'UTF-8'));
        $input->setClientField2(mb_convert_encoding( $_params['client_field_2'] , 'SJIS' , 'UTF-8'));
        $input->setClientField3(mb_convert_encoding( $_params['client_field_3'] , 'SJIS' , 'UTF-8'));
        $_params['site_id']   = $input->getSiteId();
        $_params['site_pass'] = $input->getSitePass();
        $_params['register']  = 'create transaction temporary' . ' - ';

        $this->log = new Gmopg_Log(get_class($this));
        $this->log->className_ = 'Pw_gmo_gateway';

        $_params_log = array(
            'function'     => 'create_transaction_temporary',
            'data'         => $input->toString(),
            'data_request' => $_params,
        );

        $exe = new ExecTran();
        $output = $exe->exec( $input );
        if(isset($output->map['errcode'])) {
            $_params_log['error'] = implode(" & ", $output->map);
            $this->log->error($_params_log);

            return array(
                'status'     => false,
                'error_code' => $output->map['errcode'],
                'error_info' => $output->map['errinfo'],
            );
        } else {
            $_push_info = array();

            $_push_info['order_id']         = $output->getOrderId();
            $_push_info['cardName']         = $output->getCardName();
            $_push_info['forward']          = $output->getForward();
            $_push_info['approve']          = $output->getApprovalNo();
            $_push_info['pay_method']       = $output->getMethod();
            $_push_info['pay_time']         = $output->getPayTimes();
            $_push_info['transactionId']    = $output->getTranId();
            $_push_info['tranDate']         = $output->getTranDate();
            $_push_info['checkString']      = $output->getCheckString();
            $_push_info['client_field_1']   = $output->getClientField1();
            $_push_info['client_field_2']   = $output->getClientField2();
            $_push_info['client_field_3']   = $output->getClientField3();
            $_push_info['acsUrl']           = $output->getAcsUrl();
            $_push_info['paReq']            = $output->getPaReq();
            $_push_info['md']               = $output->getMd();

            $_params_log['result'] = $_push_info;
            $this->log->info($_params_log);

            return array(
                'data'      => $_push_info,
                'status'    => true,
            );
        }
    }

    /**
     * search_trading_charge_money
     *
     * @author ngoc_tuan
     * @param array $_params
     * @since 2019-10-03
     * @return array
     * [CRE][49728][option]
     */
    public function search_trading_charge_money($_params){
        require_once('input/SearchTradeInput.php');
        require_once('tran/SearchTrade.php');
        require_once('common/Gmopg_Log.php');

        $input = new SearchTradeInput();

        $input->setShopId($_params['shop_id']);
        $input->setShopPass($_params['shop_password']);
        $input->setOrderId($_params['order_id']);

        $_params['shop_id']   = $input->getShopId();
        $_params['shop_password'] = $input->getShopPass();
        $_params['register']  = 'search_trading_charge_money ';

        $this->log = new Gmopg_Log(get_class($this));
        $this->log->className_ = 'Pw_gmo_gateway';

        $_params_log = array(
            'function'     => 'search_trading_charge_money',
            'data'         => $input->toString(),
            'data_request' => $_params,
        );
        $exe = new SearchTrade();

        $output = $exe->exec( $input );

        if(isset($output->map['errcode'])) {
            $_params_log['error'] = implode(" & ", $output->map);
            $this->log->error($_params_log);

            return array(
                'status'     => false,
                'error_code' => $output->map['errcode'],
                'error_info' => $output->map['errinfo'],
            );
        } else {
            $_push_info = array();

            $_push_info['order_id'] = $output->getOrderId();
            $_push_info['status'] = $output->getStatus();
            $_push_info['process_date'] = $output->getProcessDate();
            $_push_info['job_cd'] = $output->getJobCd();
            $_push_info['access_id'] = $output->getAccessId();
            $_push_info['access_password'] = $output->getAccessPass();
            $_push_info['item_code'] = $output->getItemCode();
            $_push_info['amount'] = $output->getAmount();
            $_push_info['tax'] = $output->getTax();
            $_push_info['site_id'] = $output->getSiteId();
            $_push_info['member_id'] = $output->getMemberId();
            $_push_info['card_no'] = $output->getCardNo();
            $_push_info['expire'] = $output->getExpire();
            $_push_info['method'] = $output->getMethod();
            $_push_info['pay_time'] = $output->getPayTimes();
            $_push_info['forward'] = $output->getForward();
            $_push_info['tran_id'] = $output->getTranId();
            $_push_info['approve'] = $output->getApprovalNo();
            $_push_info['client_field_1'] = $output->getClientField1();
            $_push_info['client_field_2'] = $output->getClientField2();
            $_push_info['client_field_3'] = $output->getClientField3();

            $_params_log['result'] = $_push_info;
            $this->log->info($_params_log);
            return array(
                'status'     => true,
                'data'      => $_push_info
            );
        }
    }

    /**
     * search_member_id
     *
     * @author ngoc_tuan
     * @param array $_params
     * @since 2019-10-03
     * @return array
     * [CRE][49728][option]
     */
    public function search_member_with_id($_params){
        require_once('input/SearchMemberInput.php');
        require_once('tran/SearchMember.php');
        require_once('common/Gmopg_Log.php');

        $input = new SearchMemberInput();

        $input->setSiteId($_params['site_id']);
        $input->setSitePass($_params['site_pass']);
        $input->setMemberId($_params['member_id']);

        $_params['site_id']   = $input->getSiteId();
        $_params['site_pass'] = $input->getSitePass();
        $_params['member_id'] = $input->getMemberId();
        $_params['register']  = 'search_member_with_id';

        $this->log = new Gmopg_Log(get_class($this));
        $this->log->className_ = 'Pw_gmo_gateway';

        $_params_log = array(
            'function'     => 'search_member_with_id',
            'data'         => $input->toString(),
            'data_request' => $_params,
        );
        $exe = new SearchMember();

        $output = $exe->exec( $input );

        if(isset($output->map['errcode'])) {
            $_params_log['error'] = implode(" & ", $output->map);
            $this->log->error($_params_log);

            return array(
                'status'     => false,
                'error_code' => $output->map['errcode'],
                'error_info' => $output->map['errinfo'],
            );
        } else {
            $_push_info = array();

            $_push_info['member_id'] = $output->getMemberId();
            $_push_info['member_name'] = $output->getMemberName();

            $_params_log['result'] = $_push_info;
            $this->log->info($_params_log);
            return array(
                'status'     => true,
                'data'      => $_push_info
            );
        }
    }

    /**
     * search_card
     *
     * @author ngoc_tuan
     * @param array $_params
     * @since 2019-10-03
     * @return array
     * [CRE][49728][option]
     */
    public function search_card($_params){
        require_once('input/SearchCardInput.php');
        require_once('tran/SearchCard.php');
        require_once('common/Gmopg_Log.php');

        $input = new SearchCardInput();

        $input->setSiteId($_params['site_id']);
        $input->setSitePass($_params['site_pass']);
        $input->setMemberId($_params['member_id']);
        $input->setSeqMode($_params['seq_mode']);
        $input->setCardSeq($_params['card_seq']);

        $_params['site_id']   = $input->getSiteId();
        $_params['site_pass'] = $input->getSitePass();
        $_params['member_id'] = $input->getMemberId();
        $_params['seq_mode'] = $input->getSeqMode();
        $_params['card_seq'] = $input->getCardSeq();
        $_params['register']  = 'search_card ';

        $this->log = new Gmopg_Log(get_class($this));
        $this->log->className_ = 'Pw_gmo_gateway';

        $_params_log = array(
            'function'     => 'search_card',
            'data'         => $input->toString(),
            'data_request' => $_params,
        );
        $exe = new SearchCard();

        $output = $exe->exec( $input );

        if(isset($output->map['errcode'])) {
            $_params_log['error'] = implode(" & ", $output->map);
            $this->log->error($_params_log);

            return array(
                'status'     => false,
                'error_code' => $output->map['errcode'],
                'error_info' => $output->map['errinfo'],
            );
        }
        elseif(count($output->getCardSeq()) > 0) {
            $_push_info_response = $_push_info_detail =array();
            $_push_info_response['card_seq'] = $output->getCardSeq();
            $_push_info_response['expire'] = $output->getExpire();
            $_push_info_response['card_no'] = $output->getCardNo();
            $_push_info_response['default_flag'] = $output->getDefaultFlag();
            $_push_info_response['card_name'] = $output->getCardName();
            $_push_info_response['card_no'] = $output->getCardNo();
            $_push_info_response['holder_name'] = $output->getHolderName();
            $_push_info_response['delete_flag'] = $output->getDeleteFlag();
            $_push_info_response['delete_flag'] = $output->getDeleteFlag();
            $_push_info_response['brand'] = $output->getBrand();
            $_push_info_response['domestic_flag'] = $output->getDomesticFlag();
            $_push_info_response['issuer_code'] = $output->getIssuerCode();
            $_push_info_response['debitPrepaidIssuerName'] = $output->getDebitPrepaidFlag();
            $_push_info_response['forwardFinal'] = $output->getForwardFinal();
            $_push_info_response['cardList'] = $output->getCardList();
            $_push_info_response['cardList'] = $output->getCardList();
            foreach ($_push_info_response as $key => $value){
                if(is_array($value)){
                    foreach ($value as $_key => $_item){
                        if(is_array($_item)){
                            foreach ($_item as $_key_item => $_value_item){
                                $_push_info_detail[$_key_item] = $_value_item;
                            }
                            continue;
                        }
                        $_push_info_detail[$key] = $_item;
                    }
                    continue;
                }
                $_push_info_detail[$key] = $value;
            }

            $_params_log['result'] = $_push_info_detail;
            $this->log->info($_params_log);

            return array(
                'status'     => true,
                'data' => $_push_info_detail,
            );
        }
    }

    /**
     * register_card_option
     *
     * @author ngoc_tuan
     * @param array $_params
     * @since 2019-08-05
     * @return array
     * [CRE][49728]
     */
    public function register_card_option($_params)
    {
        require_once('input/SaveCardInput.php');
        require_once('tran/SaveCard.php');
        require_once('common/Gmopg_Log.php');

        $input = new SaveCardInput();

        $input->setSiteId($_params['site_id']);

        $input->setSitePass($_params['site_pass']);

        $input->setMemberId($_params['member_id']);

        $input->setSeqMode($_params['seq_mode']);

        $input->setCardSeq($_params['card_seq']);

        $input->setDefaultFlag($_params['default_flag']);

        $input->setCardName(mb_convert_encoding($_params['card_name'] , 'SJIS' , 'UTF-8'));

        $input->setCardNo( $_params['card_no'] );
        $input->setCardPass( $_params['card_pass'] );
        $input->setExpire( $_params['expire'] );
        $input->setHolderName( $_params['holder_name']);
        $input->setSecurityCode( $_params['security_code']);

        $input->setSrcMemberID( $_params['src_member_id']);
        $input->setSrcCardSeq( $_params['src_card_seq']);

        $input->setUpdateType($_params['update_type']);

        $_params['site_id']   = $input->getSiteId();
        $_params['site_pass'] = $input->getSitePass();
        $_params['member_id'] = $input->getMemberId();
        $_params['seq_mode'] = $input->getSeqMode();
        $_params['card_seq'] = $input->getCardSeq();
        $_params['default_flag'] = $input->getDefaultFlag();
        $_params['card_name'] = $input->getCardName();
        $_params['card_no'] = $input->getCardNo();
        $_params['card_pass'] = $input->getCardPass();
        $_params['expire'] = $input->getExpire();
        $_params['holder_name'] = $input->getHolderName();
        $_params['security_code'] = $input->getSecurityCode();
        $_params['src_member_id'] = $input->getSrcMemberID();
        $_params['src_card_seq'] = $input->getSrcCardSeq();
        $_params['update_type'] = $input->getUpdateType();
        $_params['register']  = 'REGISTER_CARD OPTION';

        $this->log = new Gmopg_Log(get_class($this));
        $this->log->className_ = 'Pw_gmo_gateway';

        $_params_log = array(
            'function'     => 'register_card_option',
            'data'         => $input->toString(),
            'data_request' => $_params,
        );
        $exe = new SaveCard();

        $output = $exe->exec( $input );

        if(isset($output->map['errcode'])) {
            $_params_log['error'] = implode(" & ", $output->map);
            $this->log->error($_params_log);

            return array(
                'status'    => false,
                'error_code' => $output->map['errcode'],
                'error_info' => $output->map['errinfo'],
            );
        } elseif($output->cardNo != null) {
            $_push_info = array();

            foreach ($output as $key => $value){
                if($key == 'errList') {
                    break;
                }

                $_push_info[$key] = $value;
            }

            $_params_log['result'] = $_push_info;
            $this->log->info($_params_log);

            return array(
                'status'    => true,
                'card_seq' => $_push_info,
            );
        }
    }

}