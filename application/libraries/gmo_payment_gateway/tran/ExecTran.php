<?php
require_once(APPPATH.'libraries/gmo_payment_gateway/output/ExecTranOutput.php');
require_once(APPPATH.'libraries/gmo_payment_gateway/tran/BaseTran.php');
/**
 * <b>決済実行　実行クラス</b>
 *
 * @package com.gmo_pg.client
 * @subpackage tran
 * @see tranPackageInfo.php
 * @author GMO PaymentGateway
 * @version 1.0
 * @created 01-01-2008 00:00:00
 */
class ExecTran extends BaseTran {

	/**
	 * コンストラクタ
	 */
	public function __construct() {
	    parent::__construct();
	}

	/**
	 * 決済を実行する
	 *
	 * @param  ExecTranInput $input    入力パラメータ
	 * @return ExecTranOuput 出力パラメータ
	 */
	public function exec(&$input) {

        $this->log->className_ = 'ExecTran';
        $_params_log = array(
            'function'  => 'exec',
            'data'      => $input->toString()
        );
        $this->log->debug ($_params_log);

        // プロトコル呼び出し・結果取得
        $resultMap = $this->callProtocol($input->toString());

        // 戻り値がnullの場合、nullを戻す
        if (is_null($resultMap)) {
		    return null;
        }

		// ExecTranOutputを作成し、戻す
        return !isset($resultMap->map['errcode']) ? new ExecTranOutput($resultMap) : $resultMap;
	}

}
?>