<?php

/**
 * <b>ログフォーマッタ</b>
 *
 *  ログ文字列をフォーマットします
 *
 * @package com.gmo_pg.client
 * @subpackage common
 * @see commonPackageInfo.php
 * @author GMO PaymentGateway
 * @version 1.0
 * @created 01-01-2008 00:00:00
 */
class LogFormatter {

	/**
	 * ログレコードをフォーマットする
	 *
	 * @param string $className クラス名
	 * @param integer $level     ログレベル
	 * @param string $message   ログ内容
	 * @param mixed $params    置き換えパラメータ
	 * @return string フォーマットされたログメッセージ
	 */
	public function format($className,  $level, $params = null)
    {
        $data         = isset($params['data_request']) ? $params['data_request'] : '';
        $result       = isset($params['result']) ? $params['result'] : '';
        $err          = '';
        $log_content  = '';

        if(!is_array($params)) {
            $err = $params;
        } elseif (!empty($params['error'])) {
            $err = $params['error'];
        }

	    if($params['function'] == 'register_member_id')
	    {
            $log_content = "$level : " . $data['register'] . " \n";
            $log_content .= "POST: \n";
            $log_content .= "Request: \n";
            $log_content .= "\t[site_id]                         : " . $data['site_id'] . "\n";
            $log_content .= "\t[site_pass]                       : " . $data['site_pass'] . "\n";
            $log_content .= "\t[member_id]                       : " . $data['member_id'] . "\n";
            $log_content .= "\t[member_name]                     : " . $data['member_name'] . "\n";

            $log_content .= "Response: \n";
            $log_content .= "\t[member_id]                       : ". $result . "\n";
            $log_content .= "\t[error]                           : ". $err . "\n";

            $log_content = date("Y-m-d H:i:s")." \n{$log_content}------------------------------------------------------------\n";

        }
	    elseif($params['function'] == 'register_card_id') {
            $log_content  = "$level : " . $data['register'] . " \n";
            $log_content .= "POST: \n";
            $log_content .= "Request: \n";
            $log_content .= "\t[site_id]                         : " . $data['site_id']   . "\n";
            $log_content .= "\t[site_pass]                       : " . $data['site_pass'] . "\n";
            $log_content .= "\t[member_id]                       : " . $data['member_id'] . "\n";
            $log_content .= "\t[seq_mode]                        : " . $data['seq_mode']  . "\n";
            $log_content .= "\t[card_seq]                        : " . $data['card_seq']  . "\n";
            $log_content .= "\t[default_flag]                    : " . $data['default_flag'] . "\n";
            $log_content .= "\t[update_type]                     : " . $data['update_type']  . "\n";
            $log_content .= "\t[token]                           : " . $data['token']        . "\n";

            $log_content .= "Response: \n";
//            $log_content .= "\t[result]                          : " . "cardSeq => " . $result['cardSeq'] . "\n" . str_repeat("\t",10)
//                                                                    . "&cardNo => " . $result['cardNo'] . "\n" . str_repeat("\t",10)
//                                                                    . "&forward => " . $result['forward'] . "\n" . str_repeat("\t",10)
//                                                                    . "&brand => " . $result['brand'] . "\n" . str_repeat("\t",10)
//                                                                    . "&domesticFlag => " . $result['domesticFlag'] . "\n" . str_repeat("\t",10)
//                                                                    . "&issuerCode => " . $result['issuerCode'] . "\n" . str_repeat("\t",10)
//                                                                    . "&debitPrepaidIssuerName => ". $result['debitPrepaidIssuerName'] . "\n" . str_repeat("\t",10)
//                                                                    . "&debitPrepaidFlag => ". $result['debitPrepaidFlag'] . "\n" . str_repeat("\t",10)
//                                                                    . "&forwardFinal => ". $result['forwardFinal'] . "\n";\
            $log_content .= json_encode($result) ;
            $log_content .= "\t[error]                           : ". $err . "\n";

            $log_content = date("Y-m-d H:i:s") . " \n{$log_content}------------------------------------------------------------\n";
        } elseif($params['function'] == 'gmo_cancel_payment') {
            $log_content  = "$level : " . $data['register'] . " \n";
            $log_content .= "POST: \n";
            $log_content .= "Request: \n";
            $log_content .= "\t[shop_id]                         : " . $data['shop_id']   . "\n";
            $log_content .= "\t[shop_password]                   : " . $data['shop_password'] . "\n";
            $log_content .= "\t[payment_id]                      : " . $data['payment_id'] . "\n";
            $log_content .= "\t[payment_password]                : " . $data['payment_password']  . "\n";
            $log_content .= "\t[jobcd]                           : " . $data['jobcd']  . "\n";
            $log_content .= "Response: \n";
            $log_content .= "\t[result]                          : " . "accessId => " . $result['accessId'] . "\n" . str_repeat("\t",10)
                . "&accessPass => " . $result['accessPass'] . "\n" . str_repeat("\t",10)
                . "&approve => " . $result['approve'] . "\n" . str_repeat("\t",10)
                . "&brand => " . $result['brand'] . "\n" . str_repeat("\t",10)
                . "&tranId => " . $result['tranId'] . "\n" . str_repeat("\t",10)
                . "&tranDate => " . $result['tranDate'] . "\n" . str_repeat("\t",10)
                . "&forward => ". $result['forward'] . "\n" . str_repeat("\t",10). "\n";
            $log_content .= "\t[error]                           : ". $err . "\n";

            $log_content = date("Y-m-d H:i:s") . " \n{$log_content}------------------------------------------------------------\n";
        }
	    elseif($params['function'] == 'initialization_transaction'){
            $log_content  = "$level : " . $data['register'] . " \n";
            $log_content .= "POST: \n";
            $log_content .= "Request: \n";
            $log_content .= "\t[shop_id]                         : " . $data['shop_id']   . "\n";
            $log_content .= "\t[shop_password]                   : " . $data['shop_password'] . "\n";
            $log_content .= "\t[order_id]                        : " . $data['order_id'] . "\n";
            $log_content .= "\t[amount]                          : " . $data['amount']  . "\n";
            $log_content .= "\t[job_cd]                          : " . $data['job_cd']  . "\n";
            $log_content .= "Response: \n";
            if($err == ''){
                $log_content .= "\t[result]                          : " . "accessId => " . $result['accessId'] . "\n" . str_repeat("\t",10)
                    . "&accessPass => " . $result['accessPass'] . "\n";
                $log_content .= "\t[error]                           : ". $err . "\n";
            }
            else{
                $log_content .= "\t[result]                          : " . "accessId => \n" . str_repeat("\t",10)
                    . "&accessPass => \n";
                $log_content .= "\t[error]                           : ". $err . "\n";
            }


            $log_content = date("Y-m-d H:i:s") . " \n{$log_content}------------------------------------------------------------\n";
        }
	    elseif($params['function'] == 'create_transaction_temporary'){
            $log_content  = "$level : " . $data['register'] . " \n";
            $log_content .= "POST: \n";
            $log_content .= "Request: \n";
            $log_content .= "\t[site_id]                            : " . $data['site_id']   . "\n";
            $log_content .= "\t[site_pass]                          : " . $data['site_pass'] . "\n";
            $log_content .= "\t[accessId]                           : " . $data['accessId'] . "\n";
            $log_content .= "\t[accessPass]                         : " . $data['accessPass']  . "\n";
            $log_content .= "\t[order_id]                           : " . $data['order_id']  . "\n";
            $log_content .= "\t[member_id]                          : " . $data['member_id']  . "\n";
            $log_content .= "\t[card_seq]                           : " . $data['card_seq']  . "\n";
            $log_content .= "\t[pay_method]                         : " . $data['pay_method']  . "\n";
            $log_content .= "\t[pay_time]                           : " . $data['pay_time']  . "\n";
            $log_content .= "\t[client_field_1]                     : " . $data['client_field_1']  . "\n";
            $log_content .= "\t[client_field_2]                     : " . $data['client_field_2']  . "\n";
            $log_content .= "\t[client_field_3]                     : " . $data['client_field_3']  . "\n";
            $log_content .= "Response: \n";
            if($err == ''){
                $log_content .= "\t[result]                          : " . "order_id => " . $result['order_id'] . "\n" . str_repeat("\t",10)
                    . "&cardName => " . $result['cardName'] . "\n" . str_repeat("\t",10)
                    . "&forward => " . $result['forward'] . "\n" . str_repeat("\t",10)
                    . "&approve => " . $result['approve'] . "\n" . str_repeat("\t",10)
                    . "&pay_method => " . $result['pay_method'] . "\n" . str_repeat("\t",10)
                    . "&pay_time => " . $result['pay_time'] . "\n" . str_repeat("\t",10)
                    . "&transactionId => " . $result['transactionId'] . "\n" . str_repeat("\t",10)
                    . "&tranDate => " . $result['tranDate'] . "\n" . str_repeat("\t",10)
                    . "&checkString => " . $result['checkString'] . "\n" . str_repeat("\t",10)
                    . "&client_field_1 => " . $result['client_field_1'] . "\n" . str_repeat("\t",10)
                    . "&client_field_2 => " . $result['client_field_2'] . "\n" . str_repeat("\t",10)
                    . "&client_field_3 => " . $result['client_field_3'] . "\n" . str_repeat("\t",10)
                    . "&acsUrl => " . $result['acsUrl'] . "\n" . str_repeat("\t",10)
                    . "&paReq => " . $result['paReq'] . "\n" . str_repeat("\t",10)
                    . "&md => " . $result['md'] . "\n";
            }else{
                    $log_content .= "\t[result]                          : " . "order_id => \n" . str_repeat("\t",10)
                    . "&cardName => \n" . str_repeat("\t",10)
                    . "&forward => \n" . str_repeat("\t",10)
                    . "&approve => \n" . str_repeat("\t",10)
                    . "&pay_method => \n" . str_repeat("\t",10)
                    . "&pay_time => \n" . str_repeat("\t",10)
                    . "&transactionId => \n" . str_repeat("\t",10)
                    . "&tranDate => \n" . str_repeat("\t",10)
                    . "&checkString => \n" . str_repeat("\t",10)
                    . "&client_field_1 => \n" . str_repeat("\t",10)
                    . "&client_field_2 => \n" . str_repeat("\t",10)
                    . "&client_field_3 => \n" . str_repeat("\t",10)
                    . "&acsUrl => \n" . str_repeat("\t",10)
                    . "&paReq => \n" . str_repeat("\t",10)
                    . "&md => \n";
            }

            $log_content .= "\t[error]                           : ". $err . "\n";

            $log_content = date("Y-m-d H:i:s") . " \n{$log_content}------------------------------------------------------------\n";
        }
	    elseif($params['function'] == 'change_money_charge'){
            $log_content  = "$level : " . $data['register'] . " \n";
            $log_content .= "POST: \n";
            $log_content .= "Request: \n";
            $log_content .= "\t[shop_id]                            : " . $data['shop_id']   . "\n";
            $log_content .= "\t[shop_password]                      : " . $data['shop_password'] . "\n";
            $log_content .= "\t[accessId]                           : " . $data['accessId'] . "\n";
            $log_content .= "\t[accessPass]                         : " . $data['accessPass']  . "\n";
            $log_content .= "\t[job_cd]                             : " . $data['job_cd']  . "\n";
            $log_content .= "\t[amount]                             : " . $data['amount']  . "\n";
            $log_content .= "\t[tax]                                : " . $data['tax']  . "\n";
            $log_content .= "Response: \n";
            $log_content .= "\t[result]                          : " . "accessId => " . $result['accessId'] . "\n" . str_repeat("\t",10)
                . "&accessPass => " . $result['accessPass'] . "\n" . str_repeat("\t",10)
                . "&forward => " . $result['forward'] . "\n" . str_repeat("\t",10)
                . "&approve => " . $result['approve'] . "\n" . str_repeat("\t",10)
                . "&tranId => " . $result['tranId'] . "\n" . str_repeat("\t",10)
                . "&tranDate => " . $result['tranDate'] . "\n";
            $log_content .= "\t[error]                           : ". $err . "\n";

            $log_content = date("Y-m-d H:i:s") . " \n{$log_content}------------------------------------------------------------\n";
        }
	    elseif($params['function'] == 'search_trading_charge_money'){
            $log_content  = "$level : " . $data['register'] . " \n";
            $log_content .= "POST: \n";
            $log_content .= "Request: \n";
            $log_content .= "\t[shop_id]                            : " . $data['shop_id']   . "\n";
            $log_content .= "\t[shop_password]                      : " . $data['shop_password'] . "\n";
            $log_content .= "\t[order_id]                                : " . $data['order_id']  . "\n";
            $log_content .= "Response: \n";
            if($err == ''){
                $log_content .= "\t[result]                          : " . "order_id => " . $result['order_id'] . "\n" . str_repeat("\t",10)
                    . "&status => " . $result['status'] . "\n" . str_repeat("\t",10)
                    . "&process_date => " . $result['process_date'] . "\n" . str_repeat("\t",10)
                    . "&job_cd => " . $result['job_cd'] . "\n" . str_repeat("\t",10)
                    . "&access_id => " . $result['access_id'] . "\n" . str_repeat("\t",10)
                    . "&access_password => " . $result['access_password'] . "\n" . str_repeat("\t",10)
                    . "&item_code => " . $result['item_code'] . "\n" . str_repeat("\t",10)
                    . "&amount => " . $result['amount'] . "\n" . str_repeat("\t",10)
                    . "&tax => " . $result['tax'] . "\n" . str_repeat("\t",10)
                    . "&site_id => " . $result['site_id'] . "\n" . str_repeat("\t",10)
                    . "&member_id => " . $result['member_id'] . "\n" . str_repeat("\t",10)
                    . "&card_no => " . $result['card_no'] . "\n" . str_repeat("\t",10)
                    . "&expire => " . $result['expire'] . "\n" . str_repeat("\t",10)
                    . "&method => " . $result['method'] . "\n" . str_repeat("\t",10)
                    . "&pay_time => " . $result['pay_time'] . "\n" . str_repeat("\t",10)
                    . "&forward => " . $result['forward'] . "\n" . str_repeat("\t",10)
                    . "&tran_id => " . $result['tran_id'] . "\n" . str_repeat("\t",10)
                    . "&approve => " . $result['approve'] . "\n" . str_repeat("\t",10)
                    . "&approve => " . $result['approve'] . "\n" . str_repeat("\t",10)
                    . "&client_field_1 => " . $result['client_field_1'] . "\n" . str_repeat("\t",10)
                    . "&client_field_2 => " . $result['client_field_2'] . "\n" . str_repeat("\t",10)
                    . "&client_field_3 => " . $result['client_field_3'] . "\n";
            }
            else{
                $log_content .= "\t[result]                          : " . "order_id => \n" . str_repeat("\t",10)
                    . "&status => \n" . str_repeat("\t",10)
                    . "&process_date => \n" . str_repeat("\t",10)
                    . "&job_cd => \n" . str_repeat("\t",10)
                    . "&access_id => \n" . str_repeat("\t",10)
                    . "&access_password => \n" . str_repeat("\t",10)
                    . "&item_code => \n" . str_repeat("\t",10)
                    . "&amount => \n" . str_repeat("\t",10)
                    . "&tax => \n" . str_repeat("\t",10)
                    . "&site_id => \n" . str_repeat("\t",10)
                    . "&member_id => \n" . str_repeat("\t",10)
                    . "&card_no => \n" . str_repeat("\t",10)
                    . "&expire => \n" . str_repeat("\t",10)
                    . "&method => \n" . str_repeat("\t",10)
                    . "&pay_time => \n" . str_repeat("\t",10)
                    . "&forward => \n" . str_repeat("\t",10)
                    . "&tran_id => \n" . str_repeat("\t",10)
                    . "&approve => \n" . str_repeat("\t",10)
                    . "&approve => \n" . str_repeat("\t",10)
                    . "&client_field_1 => \n" . str_repeat("\t",10)
                    . "&client_field_2 => \n" . str_repeat("\t",10)
                    . "&client_field_3 => \n";
            }

            $log_content .= "\t[error]                           : ". $err . "\n";

            $log_content = date("Y-m-d H:i:s") . " \n{$log_content}------------------------------------------------------------\n";
        }
	    elseif($params['function'] == 'search_member_with_id'){
            $log_content  = "$level : " . $data['register'] . " \n";
            $log_content .= "POST: \n";
            $log_content .= "Request: \n";
            $log_content .= "\t[site_id]                            : " . $data['site_id']   . "\n";
            $log_content .= "\t[site_pass]                          : " . $data['site_pass'] . "\n";
            $log_content .= "\t[member_id]                          : " . $data['member_id']  . "\n";
            $log_content .= "Response: \n";
            if($err == ''){
                $log_content .= "\t[result]                          : " . "member_id => " . $result['member_id'] . "\n" . str_repeat("\t",10)
                    . "&member_name => " . $result['member_name'] . "\n";
            }
            else{
                $log_content .= "\t[result]                          : " . "member_id => \n" . str_repeat("\t",10)
                    . "&member_name => \n";
            }

            $log_content .= "\t[error]                           : ". $err . "\n";

            $log_content = date("Y-m-d H:i:s") . " \n{$log_content}------------------------------------------------------------\n";
        }
        elseif($params['function'] == 'search_card'){
            $log_content  = "$level : " . $data['register'] . " \n";
            $log_content .= "POST: \n";
            $log_content .= "Request: \n";
            $log_content .= "\t[site_id]                            : " . $data['site_id']   . "\n";
            $log_content .= "\t[site_pass]                          : " . $data['site_pass'] . "\n";
            $log_content .= "\t[member_id]                          : " . $data['member_id']  . "\n";
            $log_content .= "\t[seq_mode]                           : " . $data['seq_mode']  . "\n";
            $log_content .= "\t[card_seq]                           : " . $data['card_seq']  . "\n";
            $log_content .= "Response: \n";
            if($err == ''){
                $log_content .= "\t[result]                          : " . "card_seq => " . $result['card_seq'] . "\n" . str_repeat("\t",10)
                    . "&expire => " . $result['expire'] . "\n" . str_repeat("\t",10)
                    . "&card_no => " . $result['card_no'] . "\n" . str_repeat("\t",10)
                    . "&default_flag => " . $result['default_flag'] . "\n" . str_repeat("\t",10)
                    . "&card_name => " . $result['card_name'] . "\n" . str_repeat("\t",10)
                    . "&holder_name => " . $result['holder_name'] . "\n" . str_repeat("\t",10)
                    . "&delete_flag => " . $result['delete_flag'] . "\n" . str_repeat("\t",10)
                    . "&brand => " . $result['brand'] . "\n" . str_repeat("\t",10)
                    . "&domestic_flag => " . $result['domestic_flag'] . "\n" . str_repeat("\t",10)
                    . "&issuer_code => " . $result['issuer_code'] . "\n" . str_repeat("\t",10)
                    . "&debitPrepaidIssuerName => " . $result['debitPrepaidIssuerName'] . "\n" . str_repeat("\t",10)
                    . "&forwardFinal => " . $result['forwardFinal'] . "\n" . str_repeat("\t",10)
                    . "&CardSeq => " . $result['CardSeq'] . "\n" . str_repeat("\t",10)
                    . "&DefaultFlag => " . $result['DefaultFlag'] . "\n" . str_repeat("\t",10)
                    . "&CardName => " . $result['CardName'] . "\n" . str_repeat("\t",10)
                    . "&CardNo => " . $result['CardNo'] . "\n" . str_repeat("\t",10)
                    . "&Expire => " . $result['Expire'] . "\n" . str_repeat("\t",10)
                    . "&HolderName => " . $result['HolderName'] . "\n" . str_repeat("\t",10)
                    . "&DeleteFlag => " . $result['DeleteFlag'] . "\n";
            }
            else{
                $log_content .= "\t[result]                          : " . "card_seq => \n" . str_repeat("\t",10)
                    . "&expire => \n" . str_repeat("\t",10)
                    . "&card_no => \n" . str_repeat("\t",10)
                    . "&default_flag => \n" . str_repeat("\t",10)
                    . "&card_name => \n" . str_repeat("\t",10)
                    . "&holder_name => \n" . str_repeat("\t",10)
                    . "&delete_flag => \n" . str_repeat("\t",10)
                    . "&brand => \n" . str_repeat("\t",10)
                    . "&domestic_flag => \n" . str_repeat("\t",10)
                    . "&issuer_code => \n" . str_repeat("\t",10)
                    . "&debitPrepaidIssuerName => \n" . str_repeat("\t",10)
                    . "&forwardFinal => \n" . str_repeat("\t",10)
                    . "&CardSeq => \n" . str_repeat("\t",10)
                    . "&DefaultFlag => \n" . str_repeat("\t",10)
                    . "&CardName => \n" . str_repeat("\t",10)
                    . "&CardNo => \n" . str_repeat("\t",10)
                    . "&Expire => \n" . str_repeat("\t",10)
                    . "&HolderName => \n" . str_repeat("\t",10)
                    . "&DeleteFlag => \n";
            }

            $log_content .= "\t[error]                           : ". $err . "\n";

            $log_content = date("Y-m-d H:i:s") . " \n{$log_content}------------------------------------------------------------\n";
        }
        elseif($params['function'] == 'register_card_option'){
            $log_content  = "$level : " . $data['register'] . " \n";
            $log_content .= "POST: \n";
            $log_content .= "Request: \n";
            $log_content .= "\t[site_id]                         : " . $data['site_id']   . "\n";
            $log_content .= "\t[site_pass]                       : " . $data['site_pass'] . "\n";
            $log_content .= "\t[member_id]                       : " . $data['member_id'] . "\n";
            $log_content .= "\t[seq_mode]                        : " . $data['seq_mode']  . "\n";
            $log_content .= "\t[card_seq]                        : " . $data['card_seq']  . "\n";
            $log_content .= "\t[default_flag]                    : " . $data['default_flag'] . "\n";
            $log_content .= "\t[update_type]                     : " . $data['update_type']  . "\n";
            $log_content .= "\t[card_name]                       : " . $data['card_name']        . "\n";
            $log_content .= "\t[card_no]                         : " . $data['card_no']        . "\n";
            $log_content .= "\t[card_pass]                       : " . $data['card_pass']        . "\n";
            $log_content .= "\t[expire]                          : " . $data['expire']        . "\n";
            $log_content .= "\t[holder_name]                     : " . $data['holder_name']        . "\n";
            $log_content .= "\t[security_code]                   : " . $data['security_code']        . "\n";
            $log_content .= "\t[src_member_id]                   : " . $data['src_member_id']        . "\n";
            $log_content .= "\t[src_card_seq]                    : " . $data['src_card_seq']        . "\n";

            $log_content .= "Response: \n";
            $log_content .= "\t[result]                          : " . "cardSeq => " . $result['cardSeq'] . "\n" . str_repeat("\t",10)
                . "&cardNo => " . $result['cardNo'] . "\n" . str_repeat("\t",10)
                . "&forward => " . $result['forward'] . "\n" . str_repeat("\t",10)
                . "&brand => " . $result['brand'] . "\n" . str_repeat("\t",10)
                . "&domesticFlag => " . $result['domesticFlag'] . "\n" . str_repeat("\t",10)
                . "&issuerCode => " . $result['issuerCode'] . "\n" . str_repeat("\t",10)
                . "&debitPrepaidIssuerName => ". $result['debitPrepaidIssuerName'] . "\n" . str_repeat("\t",10)
                . "&debitPrepaidFlag => ". $result['debitPrepaidFlag'] . "\n" . str_repeat("\t",10)
                . "&forwardFinal => ". $result['forwardFinal'] . "\n";
            $log_content .= "\t[error]                           : ". $err . "\n";

            $log_content = date("Y-m-d H:i:s") . " \n{$log_content}------------------------------------------------------------\n";
        }
        return $log_content;
	}

}
?>