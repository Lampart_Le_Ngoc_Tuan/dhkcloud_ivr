<?php
require_once(APPPATH.'libraries/gmo_payment_gateway/common/LogFormatter.php');
/**
 * <b>独自ログクラス</b>
 *
 * GMO-PG独自のログクラス
 *
 * @package com.gmo_pg.client
 * @subpackage common
 * @see commonPackageInfo.php
 * @author GMO PaymentGateway
 * @version 1.0
 * @created 01-01-2008 00:00:00
 */
class Gmopg_Log {

	/**
	 * @var string クラス名
	 */
    public $className_;

	/**
	 * @var integer ログレベル
	 */
    public $level_;

	/**
	 * @var strign ログファイル名
	 */
    public $fileName_;

	/**
	 * @var string ログタイプ
	 */
    public $type_;

	/**
	 * @var LogFormatter ログフォーマッタ
	 */
    public $logFormatter_;

	/**
	 * コンストラクタ
	 *
	 * @param string $className クラス名称
	 */
	public function __construct($className) {
		$this->className_ = $className;
		$this->logFormatter_ = new LogFormatter();

		// 各種デフォルト値を設定
		$this->level_ = 'INFO';
		$this->fileName_ = 'gpayclient.log';
		$this->type_ = 3;

		// 初期化
		$this->init();
	}

	/**
	 * 初期化
	 */
	private function init() {

        $_gmo_info = config_item('gmo_info');
		$prop_file = null;
		$prop_name = $_gmo_info['log_file'];

        if (file_exists($prop_name)) {
            $prop_file = $prop_name;
        }

		// 定義ファイルが見つからないときは戻る
		if (is_null($prop_file)) {
			return;
		}

		// 定義ファイルの内容の解析
		$props = parse_ini_file($prop_file);

		// ログレベルの設定
		if (array_key_exists('level', $props)) {
			$this->level_ = $props['level'];
		}

		if(!is_dir('temp/logs/gmo/')) {
		    mkdir('temp/logs/gmo', 0755);
        }
		// ログファイル名の設定
		if (array_key_exists('fileName', $props)) {
			$this->fileName_ = 'temp/logs/gmo/'.$props['fileName']."-".date('Y-m-d').$props["formatFile"];
		}
		// ログタイプの設定
		if (array_key_exists('type', $props)) {
			$types = array('CONSOLE'=>0, 'FILE'=>3);
			$this->type_ = array_key_exists($props['type'], $types) ? $types[$props['type']] : 3;
		}
	}

	/**
	 * ログファイルの最終更新日付判定
	 *
	 * @param string $log_file  ログファイル名
	 */
	private function ensure($log_file) {
		// ログファイルが存在しないときは戻る
		if (false == file_exists($log_file)) {
			return;
		}

		// ログファイルの最終更新日付を取得
		$mtime = date('Ymd', filemtime($log_file));
		// 実行時の日付を取得
		$now = date('Ymd');

		// 最終更新日付が実行時日付より前のときはリネームする
		if (intval($now) > intval($mtime)) {
			rename($log_file, "$log_file.$mtime");
		}
	}

	/**
	 * ログ出力
	 *
	 * @param integer $level     ログレベル
	 * @param string $message   ログ内容
	 * @param mixed $params    置き換えパラメータ
	 */
	private function logging($level, $params = null) {
		// ログファイルの日付のチェック
		$this->ensure($this->fileName_);
		// ログの内容の構築
		$log = $this->logFormatter_->format($this->className_,$level, $params);
		// ログの出力
		error_log($log, $this->type_, $this->fileName_);
	}

	/**
	 * DEBUGレベルログ出力
	 *
	 * @param string $message    ログ内容
	 * @param mixed $params    置き換えパラメータ
	 */
	public function debug($params = null) {
	    $this->logging('DEBUG', $params);
	}

	/**
	 * WARNレベルログ出力
	 *
	 * @param string $message    ログ内容
	 * @param mixed $params    置き換えパラメータ
	 */
	public function warn($message, $params = null) {
		if ($this->isWarnEnabled()) {
			$this->logging('WARN', $message, $params);
		}
	}

	/**
	 * INFOレベルログ出力
	 *
	 * @param string $message    ログ内容
	 * @param mixed $params    置き換えパラメータ
	 */
	public function info($message, $params = null) {
	    $this->logging('INFO', $message, $params);
	}

	/**
	 * ERRORレベルログ出力
	 *
	 * @param string $message    ログ内容
	 * @param mixed $params    置き換えパラメータ
	 */
	public function error($_params = null) {
	    $this->logging('ERROR', $_params);
	}

	/**
	 * ログレベルの有効性判定
	 *
	 * <p>
	 *   パラメータのログレベルが有効であるかチェックし、判定結果のフラグが返る
	 * </p>
	 * @param integer $level ログレベル
	 * @return boolean ログ有効フラグ(true=有効、false=無効)
	 */
	private function isLogEnabled($level) {
		$levels = array('ALL'=>0, 'TRACE'=>1, 'DEBUG'=>2, 'INFO'=>3, 'WARN'=>4, 'ERROR'=>5);

		// 設定されているログのレベルを取得
		$log_level = array_key_exists($this->level_, $levels) ? $levels[$this->level_] : 0;
		// 指定のログのレベルを取得
		$info_level = array_key_exists($level, $levels) ? $levels[$level] : 0;

		// INFOのログのレベルが設定されているログのレベル以上か否か判定する
		return $log_level <= $info_level;
	}

	/**
	 * WARNレベルの有効性判定
	 * @return boolean warnログ有効フラグ(true=有効、false=無効)
	 */
	private function isWarnEnabled() {
		return $this->isLogEnabled('WARN');
	}


}
?>