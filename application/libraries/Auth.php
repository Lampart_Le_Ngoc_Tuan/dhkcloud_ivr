<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Auth
{

    protected $_ci;

    protected $_messages = array();

    protected $_namespace = '_authenticate';

    protected $_table = '';
    protected $_usernamecol = '';
    protected $_passwordcol = '';
    protected $_isuseadmincol = '';
    protected $_isuseacquisitioncol = '';
    protected $_db_fields = '';
    protected $_db_where = '';
    protected $_check_component = '';

    protected $_crypt_type = '';
    protected $_use_encrypt_key = false;
    protected $_use_encrypt_sql = true;

    protected $auth_data = array();

    protected $is_logged = false;
    protected $message = '';

    public function __construct() {
        $this->_ci =& get_instance();

        if($authenticate_data = $this->_ci->session->read($this->_namespace)) {
            if(isset($authenticate_data['logged_in']) && $authenticate_data['logged_in'] === true) {
                $this->is_logged = true;
                $this->auth_data = array(
                    'user_id'    => $authenticate_data['user_id'],
                    'username'   => $authenticate_data['username'],
                    'authority'  => $authenticate_data['authority'],
                    'name'       => $authenticate_data['name'],
                    'login_time' => $authenticate_data['login_time'],
                );
            }
        }
    }

    protected function _save() {
        $this->_ci->session->write($this->_namespace, $this->auth_data);
    }

    public function authenticate() {
//        file_put_contents(APPPATH . '/logs/authenticate.txt', json_encode($_SERVER), FILE_APPEND | LOCK_EX);
        return true;

    }

    public function get_user_id() {
        return (isset($this->auth_data['user_id'])) ? $this->auth_data['user_id'] : 0;
    }

    public function get_username() {
        return $this->auth_data['username'];
    }

    public function get_authority_data() {
        return $this->auth_data['authority_data'];
    }

    public function is_admin() {
        return ($this->auth_data['authority_id'] == 1) ? true : false;
    }

    /**
     * Initialize preferences
     *
     * @access    public
     * @param    array
     * @return    void
     */
    public function initialize($config = array()) {
        foreach ($config as $key => $val) {
            if (isset($this->{'_'.$key})) $this->{'_'.$key} = $val;
        }
    }

    /**
     * @param string $username
     * @param string $password
     * @return bool
     * @author chieng_minh
     * @since 2019-11-05
     * [CRE][49728][51918]
     */
    public function login($username = '', $password = '') {

        $this->_ci->load->model('common_model');

        $this->logout();
        if (strlen($username) == 0 || strlen($password) == 0) {
            return false;
        }

        $password = str_replace('\'', '', trim($this->encrypt($password)));

        $where = array(
            'username' => $username,
            'password' => $password,
            'disabled'  => 0,
        );

        $user = array();

        if($result = $this->_ci->db->get_where('user', $where)) {
            foreach ($result->result() as $row) {
                $user = $row;
            }
        }

        if(empty($user)) {
            $this->is_logged = false;
            return false;
        }

        $this->auth_data = array(
            'user_id'    => isset($user->id) ? $user->id : '',
            'username'   => isset($user->username) ? $user->username : '',
            'authority'  => isset($user->authority_id) ? $user->authority_id : '',
            'name'       => isset($user->name) ? $user->name : '',
            'logged_in'  => true,
            'login_time' => $this->_ci->common_model->_db_now(),
        );

        $this->_save();
        $this->is_logged = true;

        return true;
    }

    public function logout() {
        $this->auth_data = '';
        $this->_save();
        $this->is_logged = false;
        return true;
    }

    public function get_message() {
        return $this->message;
    }

    public function encrypt($str = '') {

        if(!$str) {
            return false;
        }

        set_include_path(get_include_path() . PATH_SEPARATOR . "/usr/share/pear/");
        require_once('Crypt/Blowfish.php');

        //Get secret key from config
        if (false === ($secret_key = config_item('password_salt'))) {
            show_error('Invalid password salt config.');
        }

        $crypt_blowfish     = new Crypt_Blowfish($secret_key);

        $encrypted_password = $crypt_blowfish->encrypt(base64_encode($str));

        return $this->_ci->db->escape(base64_encode($encrypted_password));
    }

    public function decrypt($str) {
        set_include_path(get_include_path() . PATH_SEPARATOR . "/usr/share/pear/");
        require_once('Crypt/Blowfish.php');

        //Get secret key from config
        $secret_key = config_item('password_salt');

        if ($secret_key == false) {
            show_error('Invalid password salt config.');
        }

        $crypt_blowfish     = new Crypt_Blowfish($secret_key);
        return base64_decode($crypt_blowfish->decrypt(base64_decode($str)));
    }

    /**
     * Create logs file for login: admin,_staff,mypage
     * @access    public
     */

    public function create_log_login($message){

        $_log_path = APPPATH .'logs/login_log-' . date('Y-m-d') . '.log';
        if(!file_exists($_log_path)){
            $message .= "";
        }
        $fp = @fopen($_log_path, FOPEN_WRITE_CREATE);
        if($fp !== FALSE){

            flock($fp, LOCK_EX);
            fwrite($fp, $message);
            flock($fp, LOCK_UN);
            fclose($fp);

            @chmod($_log_path, FILE_WRITE_MODE);
        }

    }
    public function get_date_login () {
        if ($last_time = $this->auth_data) {
            $date_time     = new DateTime($last_time['login_time']);
            $date_limit_up = new DateTime($date_time->format('Y-m-d').' ' . config_item('limit_time_next_date'));

            if ($date_time < $date_limit_up) {
                $date_limit_up->modify('-1 day');
            }

            return $date_limit_up->format('Y-m-d');
        } else {
            return false;
        }
    }

    public function get_auth_data () {
        return $this->auth_data;
    }

    public function reset_auth_data ($auth_data) {
        $this->auth_data = $auth_data;
        $this->is_logged = true;
        $this->_save();
    }
}