<?php (defined('BASEPATH')) OR exit('No direct script access allowed');


class MC_Session extends CI_Session {

    /**
     * @param string $cookie_name
     * @param array $cookie_data
     * @param null $expire
     * @author chieng_minh
     * @since 2019-11-05
     * [CRE][49728][51918]
     */
    public function write($cookie_name = '', $cookie_data = array(), $expire = null) {

        $cookie_data['last_activity'] = $this->now;

        if (is_null($cookie_data))
        {
            $cookie_data = $this->userdata;
        }

        // Serialize the userdata for the cookie
        $cookie_data = $this->_serialize($cookie_data);

        if ($this->sess_encrypt_cookie == TRUE)
        {
            $cookie_data = $this->CI->encrypt->encode($cookie_data);
        }

        $cookie_data .= hash_hmac('sha1', $cookie_data, $this->encryption_key);

        $expire = ($this->sess_expire_on_close === TRUE) ? 0 : $this->sess_expiration + time();

        $cookie_name = isset($cookie_name) ? $cookie_name : $this->sess_cookie_name;

        // Set the cookie
        setcookie(
            $cookie_name,
            $cookie_data,
            $expire,
            $this->cookie_path,
            $this->cookie_domain,
            $this->cookie_secure
        );
    }

    public function read($cookie_name) {

        static $cookie_array = array();

        if (isset($cookie_array[$cookie_name])) {
            return $cookie_array[$cookie_name];
        }

        if (false === ($session = $this->CI->input->cookie($cookie_name))) {
            $cookie_array[$cookie_name] = false;
            return false;
        }

        // Decrypt the cookie data
//        if ($this->sess_encrypt_cookie == true) {
//            $session = $this->CI->encrypt->decode($session);
//        } else {
//            // encryption was not used, so we need to check the md5 hash
//            $hash     = substr($session, strlen($session)-32); // get last 32 chars
//            $session = substr($session, 0, strlen($session)-32);
//
//            // Does the md5 hash match?  This is to prevent manipulation of session data in userspace
//            if ($hash !==  md5($session.$this->encryption_key)) {
//                $this->destroy($cookie_name);
//                return false;
//            }
//        }

        // Unserialize the session array
        $session = $this->_unserialize($session);

        // Is the session current?
        //         if (($session['last_activity'] + $this->sess_expiration) < $this->now) {
        //             $this->destroy($cookie_name);
        //             return false;
        //         }

        $cookie_array[$cookie_name] = $session;
        return $session;
    }
}