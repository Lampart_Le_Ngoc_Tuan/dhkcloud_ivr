<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_gmo_setting extends CI_Migration {

    public function up() {
        $this->dbforge->add_field(array(
            'id'                          => array(
                'type'           => 'INT',
                'constraint'     => 11,
                'unsigned'       => true,
                'auto_increment' => true,
            ),
            'site_id'                     => array(
                'type'       => 'VARCHAR',
                'constraint' => 128,
            ),
            'site_password'               => array(
                'type'       => 'VARCHAR',
                'constraint' => 128,
            ),
            'authorization_shop_id'       => array(
                'type'       => 'VARCHAR',
                'constraint' => 128,
            ),
            'authorization_shop_password' => array(
                'type'       => 'VARCHAR',
                'constraint' => 128,
            ),

            'charge_shop_id' => array(
                'type'       => 'VARCHAR',
                'constraint' => 128,
            ),

            'charge_shop_password' => array(
                'type'       => 'VARCHAR',
                'constraint' => 128,
            ),
            'lastup_contract_id'   => array(
                'type'       => 'INT',
                'constraint' => 11,
                'unsigned'   => true,
            ),
            'create_datetime'      => array(
                'type' => 'DATETIME',
            ),
            'lastup_datetime'      => array(
                'type' => 'DATETIME',
            ),
            'disabled'             => array(
                'type'       => 'TINYINT',
                'constraint' => 1,
                'unsigned'   => true,
                'default'    => '0'
            )
        ));

        $this->dbforge->add_key('id', true);

        $this->dbforge->create_table('gmo_setting');
    }

    public function down() {
        $this->dbforge->drop_table('gmo_setting');
    }
}