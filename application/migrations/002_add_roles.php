<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_roles extends CI_Migration {

    public function up() {
        $this->dbforge->add_field(array(
            'id'                 => array(
                'type'           => 'INT',
                'constraint'     => 11,
                'unsigned'       => true,
                'auto_increment' => true,
            ),
            'allow_ip'           => array(
                'type'       => 'VARCHAR',
                'constraint' => 50,
            ),
            'deny_ip'            => array(
                'type'       => 'INT',
                'constraint' => 11,
                'unsigned'   => true,
            ),
            'request_uri'        => array(
                'type'       => 'VARCHAR',
                'constraint' => 254,
            ),
            'lastup_contract_id' => array(
                'type'       => 'INT',
                'constraint' => 11,
                'unsigned'   => true,
            ),
            'create_datetime'    => array(
                'type' => 'DATETIME',
            ),
            'lastup_datetime'    => array(
                'type' => 'DATETIME',
            ),
            'disabled'           => array(
                'type'       => 'TINYINT',
                'constraint' => 1,
                'unsigned'   => true,
                'default'    => '0'
            )
        ));

        $this->dbforge->add_key('id', true);

        $this->dbforge->create_table('roles');
    }

    public function down() {
        $this->dbforge->drop_table('roles');
    }
}