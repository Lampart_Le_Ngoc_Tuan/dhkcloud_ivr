<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_api_info extends CI_Migration {

    public function up() {
        $this->dbforge->add_field(array(
            'id'                 => array(
                'type'           => 'INT',
                'constraint'     => 11,
                'unsigned'       => true,
                'auto_increment' => true,
            ),
            'user_id'            => array(
                'type'       => 'VARCHAR',
                'constraint' => 50,
            ),
            'roles_id'           => array(
                'type'       => 'INT',
                'constraint' => 11,
                'unsigned'   => true,
            ),
            'api_key'            => array(
                'type'       => 'VARCHAR',
                'constraint' => 50,
            ),
            'api_password'       => array(
                'type'       => 'VARCHAR',
                'constraint' => 50,
            ),
            'lastup_contract_id' => array(
                'type'       => 'INT',
                'constraint' => 11,
                'unsigned'   => true,
            ),
            'create_datetime'    => array(
                'type' => 'DATETIME',
            ),
            'lastup_datetime'    => array(
                'type' => 'DATETIME',
            ),
            'disabled'           => array(
                'type'       => 'TINYINT',
                'constraint' => 1,
                'unsigned'   => true,
                'default'    => '0'
            )
        ));

        $this->dbforge->add_key('id', true);

        $this->dbforge->create_table('api_info');
    }

    public function down() {
        $this->dbforge->drop_table('api_info');
    }
}