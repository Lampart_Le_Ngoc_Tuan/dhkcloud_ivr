<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_user extends CI_Migration {

    public function up() {
        $this->dbforge->add_field(array(
            'id'                => array(
                'type'           => 'INT',
                'constraint'     => 11,
                'unsigned'       => true,
                'auto_increment' => true,
            ),
            'username'      => array(
                'type'       => 'VARCHAR',
                'constraint' => 50,
            ),
            'password'       => array(
                'type'       => 'VARCHAR',
                'constraint' => 50,
            ),
            'salt'  => array(
                'type'       => 'VARCHAR',
                'constraint' => 50,
            ),
            'authority_id'          => array(
                'type'       => 'INT',
                'constraint' => 11,
                'unsigned'   => true,
            ),
            'lastup_contract_id' => array(
                'type'       => 'INT',
                'constraint' => 11,
                'unsigned'   => true,
            ),
            'create_datetime'   => array(
                'type' => 'DATETIME',
            ),
            'lastup_datetime'   => array(
                'type' => 'DATETIME',
            ),
            'disabled'          => array(
                'type'       => 'TINYINT',
                'constraint' => 1,
                'unsigned'   => true,
                'default'    => '0'
            )
        ));

        $this->dbforge->add_key('id', true);

        $this->dbforge->create_table('user');
    }

    public function down() {
        $this->dbforge->drop_table('user');
    }
}