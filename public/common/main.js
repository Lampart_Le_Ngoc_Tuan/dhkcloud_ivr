$(document).ready(function(){

    $('#get_gmo_token').on('click', function () {
        var card_info = {
            card_number  : $('input[name = card_no]').val(),
            expire       : $('#card_expired_year :selected').val() + $('#card_expired_month :selected').val(),
            name         : $('input[name = holder_name]').val(),
            cvc          : $('input[name = security_code]').val()
        };

        console.log(card_info);
        gmo_token(card_info);
    });

    function gmo_token(card_info) {

        var authorization_shop_id  = $('input[name = authorization_shop_id]').val();

        Multipayment.init(authorization_shop_id);

        var tokennumber = 1;
        Multipayment.getToken({
            cardno       : card_info.card_number,
            expire       : card_info.expire,
            securitycode : card_info.cvc,
            holdername   : card_info.name,
            tokennumber  : tokennumber
        }, add_gmo_token);
    }
});


/**
 * function add_gmo_token
 * callback function must be declare global
 * @param response
 */
function add_gmo_token(response) {
    $('<input>').attr({
        type    : 'hidden',
        id      : 'result_code_gmo_token',
        name    : 'result_code_gmo_token',
        value   : response.resultCode
    }).appendTo('form');
    console.log(response.resultCode);
    if (response.resultCode === '000') {
        $('<input>').attr({
            type  : 'hidden',
            id    : 'gmo_token',
            name  : 'gmo_token',
            value : response.tokenObject.token
        }).appendTo('form');
    }
    $('#send_to_gmo').click();
}