-- --------------------------------------------------------
-- Host:                         172.16.100.14
-- Server version:               5.1.73-log - Source distribution
-- Server OS:                    redhat-linux-gnu
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table dhkcloud_pci.api_info
CREATE TABLE IF NOT EXISTS `api_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(50) NOT NULL,
  `roles_id` int(11) unsigned NOT NULL,
  `api_key` varchar(50) NOT NULL,
  `api_password` varchar(50) NOT NULL,
  `lastup_contract_id` int(11) unsigned NOT NULL,
  `create_datetime` datetime NOT NULL,
  `lastup_datetime` datetime NOT NULL,
  `disabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table dhkcloud_pci.api_info: ~0 rows (approximately)
/*!40000 ALTER TABLE `api_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `api_info` ENABLE KEYS */;

-- Dumping structure for table dhkcloud_pci.contract_card_ivr_log
CREATE TABLE IF NOT EXISTS `contract_card_ivr_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `discernment_id` int(11) NOT NULL DEFAULT '0',
  `customer_id` int(11) NOT NULL,
  `customer_phone` varchar(50) NOT NULL,
  `status_code` varchar(4) NOT NULL,
  `save_success_flag` varchar(2) NOT NULL DEFAULT '',
  `receipt_no` varchar(254) NOT NULL,
  `gateway_type` tinyint(4) NOT NULL DEFAULT '1',
  `lastup_account_id` int(11) NOT NULL,
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disabled` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `contract_id` (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- Dumping data for table dhkcloud_pci.contract_card_ivr_log: ~12 rows (approximately)
/*!40000 ALTER TABLE `contract_card_ivr_log` DISABLE KEYS */;
INSERT INTO `contract_card_ivr_log` (`id`, `discernment_id`, `customer_id`, `customer_phone`, `status_code`, `save_success_flag`, `receipt_no`, `gateway_type`, `lastup_account_id`, `create_datetime`, `lastup_datetime`, `disabled`) VALUES
	(2, 0, 1306451, '65511113235', '', '1', 'PW201911180000000001', 2, 0, '2019-11-18 07:08:45', '2019-11-18 07:08:45', 0),
	(3, 0, 1306451, '65511113235', '', '1', 'PW201911180000000002', 2, 0, '2019-11-19 04:26:07', '2019-11-19 04:26:07', 0),
	(4, 0, 1306451, '65511113235', '', '1', 'PW201911180000000003', 2, 0, '2019-11-18 07:39:18', '2019-11-18 07:39:18', 0),
	(5, 0, 1306451, '65511113235', '', '0', 'PW201911180000000004', 2, 0, '2019-11-18 07:41:21', '2019-11-18 07:41:21', 0),
	(6, 0, 1306451, '65511113235', '', '1', 'PW201911180000000005', 2, 0, '2019-11-18 08:11:48', '2019-11-18 08:11:48', 0),
	(7, 0, 1306451, '65511113235', '', '', 'PW201911180000000006', 2, 0, '2019-11-18 08:42:23', '0000-00-00 00:00:00', 0),
	(8, 0, 1306451, '65511113235', '', '', 'PW201911180000000007', 2, 0, '2019-11-18 08:43:09', '0000-00-00 00:00:00', 0),
	(9, 0, 1306451, '65511113235', '', '', 'PW201911180000000008', 2, 0, '2019-11-18 08:44:52', '0000-00-00 00:00:00', 0),
	(10, 0, 1306451, '65511113235', '', '1', 'PW201911180000000009', 2, 0, '2019-11-18 09:11:03', '2019-11-18 09:11:03', 0),
	(11, 0, 1306400, '65511113235', '', '', 'PW201911180000000010', 2, 0, '2019-11-18 09:19:16', '0000-00-00 00:00:00', 0),
	(12, 0, 1306451, '65511113235', '', '', 'PW201911180000000011', 2, 0, '2019-11-18 10:05:45', '0000-00-00 00:00:00', 0),
	(13, 0, 1306451, '65511113235', '', '', 'PW201911180000000012', 2, 0, '2019-11-18 10:06:07', '0000-00-00 00:00:00', 0),
	(14, 0, 1306451, '65511113235', '', '', 'PW201911190000000001', 2, 0, '2019-11-19 02:52:40', '0000-00-00 00:00:00', 0),
	(15, 0, 1306391, '00000000000', '', '1', 'PW201911190000000002', 2, 0, '2019-11-19 06:26:57', '2019-11-19 06:26:57', 0),
	(16, 0, 1306410, '00000000000', '', '', 'PW201911190000000003', 2, 0, '2019-11-19 06:49:17', '0000-00-00 00:00:00', 0),
	(17, 0, 1306451, '65511113235', '', '', 'PW201911190000000004', 2, 0, '2019-11-19 07:58:53', '0000-00-00 00:00:00', 0);
/*!40000 ALTER TABLE `contract_card_ivr_log` ENABLE KEYS */;

-- Dumping structure for table dhkcloud_pci.contract_gmo
CREATE TABLE IF NOT EXISTS `contract_gmo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` varchar(50) NOT NULL DEFAULT '',
  `contract_card_ivr_log_id` int(11) NOT NULL,
  `error_code` varchar(50) DEFAULT '',
  `error_info` varchar(50) DEFAULT '',
  `lastup_account_id` int(11) NOT NULL,
  `create_datetime` datetime NOT NULL,
  `lastup_datetime` datetime NOT NULL,
  `disabled` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table dhkcloud_pci.contract_gmo: ~5 rows (approximately)
/*!40000 ALTER TABLE `contract_gmo` DISABLE KEYS */;
INSERT INTO `contract_gmo` (`id`, `member_id`, `contract_card_ivr_log_id`, `error_code`, `error_info`, `lastup_account_id`, `create_datetime`, `lastup_datetime`, `disabled`) VALUES
	(1, 'CCID1306451', 2, '', '', 0, '2019-11-18 07:08:42', '0000-00-00 00:00:00', 0),
	(2, 'CCID1306451', 4, 'E01', 'E01390010', 0, '2019-11-18 07:39:14', '0000-00-00 00:00:00', 0),
	(3, 'CCID1306451', 5, 'E01', 'E01390010', 0, '2019-11-18 07:41:20', '0000-00-00 00:00:00', 0),
	(4, 'CCID1306451', 6, 'E01', 'E01390010', 0, '2019-11-18 08:11:45', '0000-00-00 00:00:00', 0),
	(5, 'CCID1306451', 10, 'E01', 'E01390010', 0, '2019-11-18 09:11:00', '0000-00-00 00:00:00', 0),
	(6, 'CCID1306451', 3, 'E01', 'E01390010', 0, '2019-11-19 04:26:03', '0000-00-00 00:00:00', 0),
	(7, 'CCID1306391', 15, 'E01', 'E01390010', 0, '2019-11-19 06:26:56', '0000-00-00 00:00:00', 0);
/*!40000 ALTER TABLE `contract_gmo` ENABLE KEYS */;

-- Dumping structure for table dhkcloud_pci.gmo_card_info
CREATE TABLE IF NOT EXISTS `gmo_card_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contract_card_ivr_log_id` int(11) NOT NULL,
  `contract_gmo_id` int(11) NOT NULL,
  `card_status` tinyint(4) NOT NULL DEFAULT '0',
  `card_seq` varchar(4) NOT NULL DEFAULT '',
  `error_code` varchar(50) NOT NULL DEFAULT '',
  `error_info` varchar(50) NOT NULL DEFAULT '',
  `create_datetime` datetime NOT NULL,
  `lastup_datetime` datetime NOT NULL,
  `lastup_account_id` int(11) NOT NULL DEFAULT '0',
  `disabled` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `contract_card_ivr_log_id` (`contract_card_ivr_log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table dhkcloud_pci.gmo_card_info: ~5 rows (approximately)
/*!40000 ALTER TABLE `gmo_card_info` DISABLE KEYS */;
INSERT INTO `gmo_card_info` (`id`, `contract_card_ivr_log_id`, `contract_gmo_id`, `card_status`, `card_seq`, `error_code`, `error_info`, `create_datetime`, `lastup_datetime`, `lastup_account_id`, `disabled`) VALUES
	(1, 2, 1, 1, '0', '', '', '2019-11-18 07:08:45', '0000-00-00 00:00:00', 0, 0),
	(2, 4, 2, 1, '0', '', '', '2019-11-18 07:39:18', '0000-00-00 00:00:00', 0, 0),
	(3, 5, 3, 0, '', 'E61', 'E61010002', '2019-11-18 07:41:21', '0000-00-00 00:00:00', 0, 0),
	(4, 6, 4, 1, '0', '', '', '2019-11-18 08:11:48', '0000-00-00 00:00:00', 0, 0),
	(5, 10, 5, 1, '0', '', '', '2019-11-18 09:11:03', '0000-00-00 00:00:00', 0, 0),
	(6, 3, 6, 1, '0', '', '', '2019-11-19 04:26:07', '0000-00-00 00:00:00', 0, 0),
	(7, 15, 7, 1, '1', '', '', '2019-11-19 06:26:57', '0000-00-00 00:00:00', 0, 0);
/*!40000 ALTER TABLE `gmo_card_info` ENABLE KEYS */;

-- Dumping structure for table dhkcloud_pci.gmo_setting
CREATE TABLE IF NOT EXISTS `gmo_setting` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` varchar(128) NOT NULL,
  `site_password` varchar(128) NOT NULL,
  `authorization_shop_id` varchar(128) NOT NULL,
  `authorization_shop_password` varchar(128) NOT NULL,
  `charge_shop_id` varchar(128) NOT NULL,
  `charge_shop_password` varchar(128) NOT NULL,
  `lastup_contract_id` int(11) unsigned NOT NULL,
  `create_datetime` datetime NOT NULL,
  `lastup_datetime` datetime NOT NULL,
  `disabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table dhkcloud_pci.gmo_setting: ~1 rows (approximately)
/*!40000 ALTER TABLE `gmo_setting` DISABLE KEYS */;
INSERT INTO `gmo_setting` (`id`, `site_id`, `site_password`, `authorization_shop_id`, `authorization_shop_password`, `charge_shop_id`, `charge_shop_password`, `lastup_contract_id`, `create_datetime`, `lastup_datetime`, `disabled`) VALUES
	(1, 'tsite00035367', 'nttdrw54', 'tshop00039942', 'q1hafm2f', 'tshop00039942', 'q1hafm2f', 0, '2019-11-18 00:00:00', '0000-00-00 00:00:00', 0);
/*!40000 ALTER TABLE `gmo_setting` ENABLE KEYS */;

-- Dumping structure for table dhkcloud_pci.pw_gmo_response
CREATE TABLE IF NOT EXISTS `pw_gmo_response` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gmo_setting_id` int(11) NOT NULL DEFAULT '0',
  `contract_card_ivr_log_id` int(11) NOT NULL,
  `gmo_member_id` varchar(50) NOT NULL DEFAULT '',
  `gmo_member_name` varchar(50) NOT NULL,
  `seq_mode` tinyint(1) NOT NULL DEFAULT '1',
  `card_seq` varchar(50) DEFAULT '',
  `default_flag` tinyint(4) DEFAULT '1',
  `card_name` int(11) NOT NULL,
  `holder_name` int(11) NOT NULL,
  `update_type` varchar(2) DEFAULT '',
  `is_member` int(11) NOT NULL,
  `lastup_account_id` int(11) NOT NULL,
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disabled` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- Dumping data for table dhkcloud_pci.pw_gmo_response: ~12 rows (approximately)
/*!40000 ALTER TABLE `pw_gmo_response` DISABLE KEYS */;
INSERT INTO `pw_gmo_response` (`id`, `gmo_setting_id`, `contract_card_ivr_log_id`, `gmo_member_id`, `gmo_member_name`, `seq_mode`, `card_seq`, `default_flag`, `card_name`, `holder_name`, `update_type`, `is_member`, `lastup_account_id`, `create_datetime`, `lastup_datetime`, `disabled`) VALUES
	(4, 1, 2, 'CCID1306451', '', 1, '', 1, 0, 0, '', 0, 1, '2019-11-18 07:07:49', '0000-00-00 00:00:00', 0),
	(5, 1, 3, 'CCID1306451', '', 1, '', 1, 0, 0, '', 0, 1, '2019-11-18 07:09:11', '0000-00-00 00:00:00', 0),
	(6, 1, 4, 'CCID1306451', '', 1, '', 1, 0, 0, '', 0, 0, '2019-11-18 07:39:11', '0000-00-00 00:00:00', 0),
	(7, 1, 5, 'CCID1306451', '', 1, '', 1, 0, 0, '', 0, 1, '2019-11-18 07:39:25', '0000-00-00 00:00:00', 0),
	(8, 1, 6, 'CCID1306451', '', 1, '', 1, 0, 0, '', 0, 0, '2019-11-18 08:11:34', '0000-00-00 00:00:00', 0),
	(9, 1, 7, 'CCID1306451', '', 1, '', 1, 0, 0, '', 0, 0, '2019-11-18 08:42:23', '0000-00-00 00:00:00', 0),
	(10, 1, 8, 'CCID1306451', '', 1, '', 1, 0, 0, '', 0, 0, '2019-11-18 08:43:09', '0000-00-00 00:00:00', 0),
	(11, 1, 9, 'CCID1306451', '', 1, '', 1, 0, 0, '', 0, 0, '2019-11-18 08:44:52', '0000-00-00 00:00:00', 0),
	(12, 1, 10, 'CCID1306451', '', 1, '', 1, 0, 0, '', 0, 0, '2019-11-18 08:46:30', '0000-00-00 00:00:00', 0),
	(13, 1, 11, 'CCID1306400', '', 1, '', 1, 0, 0, '', 1, 0, '2019-11-18 09:19:16', '0000-00-00 00:00:00', 0),
	(14, 1, 12, 'CCID1306451', '', 1, '', 1, 0, 0, '', 0, 0, '2019-11-18 10:05:45', '0000-00-00 00:00:00', 0),
	(15, 1, 13, 'CCID1306451', '', 1, '', 1, 0, 0, '', 0, 0, '2019-11-18 10:06:07', '0000-00-00 00:00:00', 0),
	(16, 1, 14, 'CCID1306451', '', 1, '', 1, 0, 0, '', 0, 0, '2019-11-19 02:52:40', '0000-00-00 00:00:00', 0),
	(17, 1, 15, 'CCID1306391', '', 1, '', 1, 0, 0, '', 1, 0, '2019-11-19 06:25:37', '0000-00-00 00:00:00', 0),
	(18, 1, 16, 'CCID1306410', '', 1, '', 1, 0, 0, '', 0, 0, '2019-11-19 06:49:17', '0000-00-00 00:00:00', 0),
	(19, 1, 17, 'CCID1306451', '', 1, '', 1, 0, 0, '', 0, 0, '2019-11-19 07:58:53', '0000-00-00 00:00:00', 0);
/*!40000 ALTER TABLE `pw_gmo_response` ENABLE KEYS */;

-- Dumping structure for table dhkcloud_pci.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `allow_ip` varchar(50) NOT NULL,
  `deny_ip` int(11) unsigned NOT NULL,
  `request_uri` varchar(254) NOT NULL,
  `lastup_contract_id` int(11) unsigned NOT NULL,
  `create_datetime` datetime NOT NULL,
  `lastup_datetime` datetime NOT NULL,
  `disabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table dhkcloud_pci.roles: ~0 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for table dhkcloud_pci.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `authority_id` int(11) unsigned NOT NULL,
  `lastup_contract_id` int(11) unsigned NOT NULL,
  `create_datetime` datetime NOT NULL,
  `lastup_datetime` datetime NOT NULL,
  `disabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table dhkcloud_pci.user: ~1 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `username`, `password`, `name`, `authority_id`, `lastup_contract_id`, `create_datetime`, `lastup_datetime`, `disabled`) VALUES
	(1, 'minhchieng', 'Szy+x/Mf4Sks4DrQtEXikQ==', 'Minh Chieng', 1, 1, '2019-04-18 13:05:56', '0000-00-00 00:00:00', 0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
